<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/', 'Auth\LoginController@showLoginForm');

Auth::routes();

Route::get('/dashboard', 'DashboardController@index')->name('dashboard');

Route::get('/admin/edit', 'UsersController@edit')->middleware('auth');
Route::resource('clients', 'ClientsController')->middleware('auth');
Route::resource('websites', 'WebsitesController')->middleware('auth');
Route::resource('domains', 'DomainsController')->middleware('auth');

Route::get('ssl_domains/get/{website}', 'SSLsController@websiteFilter')->middleware('auth');
Route::get('hosting_domains/get/{website}', 'HostingsController@websiteFilter')->middleware('auth');

Route::resource('hostings', 'HostingsController')->middleware('auth');
Route::resource('maintenances', 'MaintenancesController')->middleware('auth');
Route::resource('ssls', 'SSLsController')->middleware('auth');

Route::get('settings', 'SettingsController@edit')->middleware('auth');
Route::patch('settings', 'SettingsController@update')->middleware('auth');