<?php

use Illuminate\Http\Request;
use App\Domain;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/send', function ()
{

		$domain = Domain::first();

        Mail::send('emails.domain-renewals', ['domain' => $domain, 'expiring' => 'soon'], function ($message)
        {

            // $message->from('me@gmail.com', 'Amol Naik');
            $message->to('amolbnaik@yahoo.co.in')->subject('WEBPRO: Renewal Reminder');

        });

        return response()->json(['message' => 'Request completed']);

});