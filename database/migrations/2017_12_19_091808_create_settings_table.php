<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->text('description');
            $table->string('logo');
            $table->string('company_name');
            $table->string('company_url');
            $table->string('company_email');
            $table->boolean('auto_reminders');
            // $table->string('mail_host');
            // $table->unsignedSmallInteger('mail_port');
            // $table->string('mail_username');
            // $table->string('mail_password');
            // $table->string('mail_encryption');
            // $table->string('mail_from_name');
            // $table->string('mail_from_address');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
