<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hostings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('website_id')->unsigned();
            $table->integer('domain_id')->unsigned();
            $table->string('package_name');
            $table->text('package_details');
            $table->date('purchase_date');
            $table->date('expiry_date');
            $table->boolean('email_accounts');
            $table->integer('disk_space');
            $table->integer('bandwidth');
            $table->string('nameserver_1');
            $table->string('nameserver_2');
            $table->string('control_panel_url');
            $table->string('control_panel_username');
            $table->string('control_panel_pass');
            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->foreign('website_id')
                    ->references('id')
                    ->on('websites')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('domain_id')
                    ->references('id')
                    ->on('domains')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hostings');
    }
}
