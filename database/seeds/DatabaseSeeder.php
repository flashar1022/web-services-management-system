<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@example.com',
            'password' => Hash::make(123456),
        ]);

        DB::table('settings')->insert([
            'title' => 'WebPro | Website management app',
            'description' => 'WebPro is a Website management app that allows you to send renewal reminders for domains, ssls, hostings renewals  to your clients.',
            'logo' => 'webpro.png',
            'company_name' => 'YourCompanyName',
            'company_url' => 'https://example.com',
            'company_email' => 'admin@example.com',
            'auto_reminders' => true
        ]);

        // $this->call(UsersTableSeeder::class);
        // $this->call(ClientsTableSeeder::class);
    }
}
