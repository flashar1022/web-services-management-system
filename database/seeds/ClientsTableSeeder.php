<?php

use Illuminate\Database\Seeder;

class ClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Client::class, 15)->create()->each(function ($u) {
           	$website = $u->websites()->save(factory(App\Website::class)->make());

            $website->domains()->save(factory(App\Domain::class)->make());
            $website->maintenance()->save(factory(App\Maintenance::class)->make());

            // $website->ssls()->save(factory(App\SSL::class)->make());
            // $website->hosting()->save(factory(App\Hosting::class)->make());
        });
    }
}
