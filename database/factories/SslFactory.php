<?php

use Faker\Generator as Faker;

$factory->define(App\SSL::class, function (Faker $faker) {
    return [
        'provider_name' => $faker->company,
        'details' => $faker->text,
        'purchase_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'expiry_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '50 days')->format('Y-m-d'),
        'control_panel_url' => $faker->url,
        'control_panel_username' => $faker->userName,
        'control_panel_pass' => $faker->password
    ];
});
