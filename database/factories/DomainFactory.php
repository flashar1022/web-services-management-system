<?php
// use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(App\Domain::class, function (Faker $faker) {

// $dateTimeBetween = $faker->dateTimeThisYear($max = 'now', $timezone = 'UTC');

// $newDate = Carbon::create($dateTimeBetween);
// dd($newDate->toDateString);

    return [
        'name' => $faker->domainName,
        'purchase_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'expiry_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '50 days')->format('Y-m-d'),
        'registrar_name' => $faker->company,
        'control_panel_url' => $faker->url,
        'control_panel_username' => $faker->userName,
        'control_panel_pass' => $faker->password
    ];
});
