<?php

use Faker\Generator as Faker;

$factory->define(App\Setting::class, function (Faker $faker) {
    return [
        'title' => $faker->company,
        'logo' => 'logo.png',
        'company_name' => $faker->company,
        'company_email' => 'admin@example.com'  
    ];
});
