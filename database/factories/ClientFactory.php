<?php

use Faker\Generator as Faker;

$factory->define(App\Client::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'company_name' => $faker->company,
        'job_title' => $faker->jobTitle,
        'phone' => $faker->randomNumber(),
        'mobile' => $faker->randomNumber(),
        'address' => $faker->address,
        'client_note' => $faker->text,
        'email' => $faker->unique()->safeEmail,
    ];
});
