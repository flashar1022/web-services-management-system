<?php

use Faker\Generator as Faker;

$factory->define(App\Hosting::class, function (Faker $faker) {
    return [
        'package_name' => $faker->firstNameFemale,
        'package_details' => $faker->text,
        'purchase_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'expiry_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '50 days')->format('Y-m-d'),
        'email_accounts' => $faker->boolean,
        'disk_space' => $faker->randomDigitNotNull,
        'bandwidth' => $faker->randomDigitNotNull,
        'nameserver_1' => $faker->ipv4,
        'nameserver_2' => $faker->ipv4,
        'control_panel_url' => $faker->url,
        'control_panel_username' => $faker->userName,
        'control_panel_pass' => $faker->password
    ];
});
