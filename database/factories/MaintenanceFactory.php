<?php

use Faker\Generator as Faker;

$factory->define(App\Maintenance::class, function (Faker $faker) {
    return [
        'details' => $faker->text,
        'start_date' => $faker->date($format = 'Y-m-d', $max = 'now'),
        'expiry_date' => $faker->dateTimeBetween($startDate = 'now', $endDate = '50 days')->format('Y-m-d'),
        'admin_url' => $faker->url,
        'admin_username' => $faker->userName,
        'admin_pass' => $faker->password
    ];
});
