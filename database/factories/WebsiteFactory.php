<?php

use Faker\Generator as Faker;

$factory->define(App\Website::class, function (Faker $faker) {
    return [
        'name' => $faker->domainName,
        'website_type' => $faker->randomElement($array = ['Static', 'CMS', 'Custom']),
        'have_domains' => $faker->boolean,
        'has_hosting' => $faker->boolean,
        'has_email' => $faker->boolean,
        'has_amc' => $faker->boolean,
        'has_ssl' => $faker->boolean
    ];
});
