<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class SSL extends Model
{

    protected $table = 'ssls';

    protected $fillable = [
        'website_id',
        'domain_id',
        'provider_name',
        'details',
        'purchase_date',
        'expiry_date',
        'control_panel_url',
        'control_panel_username',
        'control_panel_pass'
    ];

    public function website()
    {
    return $this->belongsTo(Website::class);
    }

    public function domain()
    {
    return $this->belongsTo(Domain::class);
    }

    public static function sslRenewalsThisMonth()
    {
        return static::whereYear('expiry_date', Carbon::now()->year)
        ->whereMonth('expiry_date', Carbon::now()->month)
        ->where('expiry_date', '>=', Carbon::today())
        ->get();
    }

}
