<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable = ['name', 'company_name', 'job_title', 'email', 'phone', 'mobile', 'address', 'client_note'];


    public function websites()
    {
        return $this->hasMany(Website::class);
    }


	public function domains()
    {
        return $this->hasManyThrough(Domain::class, Website::class);
    }

	public function hosting()
    {
        return $this->hasOne(Hosting::class, Website::class);
    }

	public function maintenance()
    {
        return $this->hasOne(Maintenance::class, Website::class);
    }

	public function ssls()
    {
        return $this->hasManyThrough(SSL::class, Website::class);
    }

}
