<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Hosting;

class HostingRenewalEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $hosting;
    public $renewalInDays;

    public function __construct(Hosting $hosting, $renewalInDays)
    {
        $this->hosting = $hosting;
        $this->renewalInDays = $renewalInDays;

        // dd($renewalInDays);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.hosting-renewals')
            ->with([
                'hosting' => $this->hosting,
                'expiring' => $this->renewalInDays])
            ->subject('Hosting renewal reminder: Hosting for ' . $this->hosting->domain->name . ' ' . $this->renewalInDays);
    }
}
