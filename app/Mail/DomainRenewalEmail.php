<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Domain;

class DomainRenewalEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $domain;
    public $renewalInDays;

    public function __construct(Domain $domain, $renewalInDays)
    {
        $this->domain = $domain;
        $this->renewalInDays = $renewalInDays;

        // dd($renewalInDays);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.domain-renewals')
            ->with([
                'domain' => $this->domain,
                'expiring' => $this->renewalInDays])
            ->subject('Domain renewal reminder: ' . $this->domain->name . ' ' . $this->renewalInDays);
    }
}
