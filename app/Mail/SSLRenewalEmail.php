<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\SSL;

class SSLRenewalEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $ssl;
    public $renewalInDays;

    public function __construct(SSL $ssl, $renewalInDays)
    {
        $this->ssl = $ssl;
        $this->renewalInDays = $renewalInDays;

        // dd($renewalInDays);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.ssl-renewals')
            ->with([
                'ssl' => $this->ssl,
                'expiring' => $this->renewalInDays])
            ->subject('SSL renewal reminder: SSL for ' . $this->ssl->domain->name . ' ' . $this->renewalInDays);
    }
}
