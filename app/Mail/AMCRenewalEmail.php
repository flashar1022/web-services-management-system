<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Maintenance;

class AMCRenewalEmail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $maintenance;
    public $renewalInDays;

    public function __construct(Maintenance $maintenance, $renewalInDays)
    {
        $this->maintenance = $maintenance;
        $this->renewalInDays = $renewalInDays;

        // dd($renewalInDays);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.maintenance-renewals')
            ->with([
                'maintenance' => $this->maintenance,
                'expiring' => $this->renewalInDays])
            ->subject('Maintenance renewal reminder: Maintenance for ' . $this->maintenance->website->name . ' ' . $this->renewalInDays);
    }
}
