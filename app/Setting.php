<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = [
    	'title',
    	'description',
    	'logo',
    	'company_name',
    	'company_url',
    	'company_email',
        'auto_reminders'
        // 'mail_driver',
        // 'mail_host',
        // 'mail_port',
        // 'mail_username',
        // 'mail_password',
        // 'mail_encryption',
        // 'mail_from_name',
        // 'mail_from_address'
	];
}
