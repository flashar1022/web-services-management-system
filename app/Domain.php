<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Domain extends Model
{
    protected $fillable = [
        'website_id',
        'name',
        'purchase_date',
        'expiry_date',
        'registrar_name',
        'control_panel_url',
        'control_panel_username',
        'control_panel_pass'
    ];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    public function ssl()
    {
        return $this->hasOne(SSL::class);
    }

    public function hosting()
    {
        return $this->hasOne(Hosting::class);
    }

    public static function domainRenewalsThisMonth()
    {
        return static::whereYear('expiry_date', Carbon::now()->year)
            ->whereMonth('expiry_date', Carbon::now()->month)
            ->where('expiry_date', '>=', Carbon::today())
            ->get();
    }

}