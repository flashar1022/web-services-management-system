<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{

/*
    public function index()
    {
        $users = User::latest()->paginate(10);

        return view('users.index', compact('users'))->with('i', (request()->input('page', 1) - 1) * 10);
    }


    public function create()
    {
        return view('users.create');
    }


    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
        User::create(request()->all());
        return redirect('users');
    }


    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }
*/


    public function edit()
    {
        $user = Auth::user();
        return view('users.edit', compact('user'));
    }


    public function update(Request $request, User $user)
    {

        request()->validate([
            // 'name' => 'required|string|max:255',
            // 'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

         $request->user()->fill([
            'password' => Hash::make($request->password)
        ])->save();

        if(Auth::user()->id == $user->id){
            Auth::logout();
            return redirect('/login');
        } else {
            return redirect('users');
        }


    }

    public function destroy(User $user)
    {
        User::destroy($user->id);

        return redirect('users');
    }
}