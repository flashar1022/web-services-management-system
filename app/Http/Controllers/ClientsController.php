<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;

class ClientsController extends Controller
{

    public function index()
    {
        $clients = Client::latest()->paginate(10);

        return view('clients.index', compact('clients'))->with('i', (request()->input('page', 1) - 1) * 10);
    }



    public function create()
    {
        return view('clients.create');
    }


    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required|max:255',
            'company_name' => 'required|max:255',
            'job_title' => 'required|max:255',
            'email' => 'required|unique:clients|max:255',
            'phone' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'client_note' => 'required'
        ]);

        Client::create(request()->all());

        return redirect('clients')->with('status', 'Client added successfully.');

    }



/*

    ROUTE MODEL BINDING

    param name should be same as wild card name

*/

    public function show(Client $client)
    {
        return view('clients.show', compact('client'));
    }



    public function edit(Client $client)
    {
        return view('clients.edit', compact('client'));
    }



    public function update(Request $request, Client $client)
    {
        request()->validate([
            'name' => 'required',
            'company_name' => 'required',
            'job_title' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'mobile' => 'required',
            'address' => 'required',
            'client_note' => 'required'
        ]);

        $client->update(request()->all());

        return redirect("clients")->with('status', 'Client updated successfully.');
    }



    public function destroy(Client $client)
    {


        Client::destroy($client->id);
        // $clientsModels = $client->with(['websites.domains', 'websites.ssls', 'websites.hosting', 'websites.maintenance'])->first()->delete();

        return redirect('clients')->with('status', 'Client deleted successfully.');
    }
}