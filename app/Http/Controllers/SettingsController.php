<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{

    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show(Setting $setting)
    {
        //
    }


    public function edit()
    {
        $setting = Setting::first();

        return view('settings.settings', compact('setting'));
    }

    public function update(Request $request)
    {

        $setting = Setting::first();

        request()->validate([
            'title' => 'required|max:255',
            'logo' => 'required|image|mimes:jpg,jpeg,png,gif|max:2048',
            'company_name' => 'required|max:255',
            'company_url' => 'required|url|max:255',
            'company_email' => 'required|email|max:255',
            'auto_reminders' =>  'required',
            // 'mail_driver' => 'required',
            // 'mail_host' => 'required',
            // 'mail_port' => 'required',
            // 'mail_username' => 'required',
            // 'mail_password' => 'required',
            // 'mail_encryption' => 'required',
            // 'mail_from_name' => 'required',
            // 'mail_from_address' => 'required',
        ]);

        request()->logo->move(public_path('assets'), $request->logo->getClientOriginalName());

        $data = request()->all();

        if ($data['auto_reminders'] === '1') {
            $data['auto_reminders'] = true;
        } elseif($data['auto_reminders'] === '0') {
            $data['auto_reminders'] = false;
        }

        $data['logo'] = request()->logo->getClientOriginalName();

        $setting = $setting->update($data);

        return back()->with('status','Settings updated successfully.');

    }

    public function destroy(Setting $setting)
    {
        //
    }
}
