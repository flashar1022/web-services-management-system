<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Domain;
use App\Hosting;
use App\Maintenance;
use App\SSL;
use App\Website;
use App\Client;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $stats['clients']['total'] = Client::count();
        $stats['websites']['total'] = Website::count();

        $stats['domains']['total'] = Domain::count();
        $stats['domains']['expired'] = Domain::where('expiry_date', '<', Carbon::now())->count();
        $stats['domains']['exp_percent'] = ($stats['domains']['total'] > 0) ? number_format( $stats['domains']['expired']*100/$stats['domains']['total'], 2) : 0;

        $stats['ssls']['total'] = SSL::count();
        $stats['ssls']['expired'] = SSL::where('expiry_date', '<', Carbon::today())->count();
        $stats['ssls']['exp_percent'] = ($stats['ssls']['total'] > 0) ? number_format( $stats['ssls']['expired']*100/$stats['ssls']['total'], 2) : 0;


        $stats['hostings']['total'] = Hosting::count();
        $stats['hostings']['expired'] = Hosting::where('expiry_date', '<', Carbon::now())->count();
        $stats['hostings']['exp_percent'] = ($stats['hostings']['total'] > 0) ? number_format( $stats['hostings']['expired']*100/$stats['hostings']['total'], 2) : 0;

        $stats['amcs']['total'] = Maintenance::count();
        $stats['amcs']['expired'] = Maintenance::where('expiry_date', '<', Carbon::now())->count();
        $stats['amcs']['exp_percent'] = ($stats['amcs']['total'] > 0) ? number_format( $stats['amcs']['expired']*100/$stats['amcs']['total'], 2) : 0;

       return view('dashboard', compact('stats'));

        // $websites = Website::with(['domains', 'hosting', 'ssls', 'maintenance'])->get();

        // $clients = Client::with(['websites.domains', 'websites.hosting', 'websites.ssls', 'websites.maintenance'])->get();

/*

        //###################################
        //   WEBSITES: RENEWALS THIS MONTH
        //###################################

        $websites = Website::with([
            'domains' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month)->orderBy('expiry_date');
            },
            'ssls' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month)->orderBy('expiry_date');
            },
            'hosting' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month);
            },
            'maintenance' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month);
            }
        ])->get();

        dd($websites);
*/


/*

        //##################################
        //   CLIENTS: RENEWALS THIS MONTH
        //##################################

        $clientsWithRenewals = Client::with([
            'websites.domains' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month)->orderBy('expiry_date')
                ->where('expiry_date', '>=', Carbon::today());
            },
            'websites.ssls' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month)->orderBy('expiry_date')
                ->where('expiry_date', '>=', Carbon::today());
            },
            'websites.hosting' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month)
                ->where('expiry_date', '>=', Carbon::today());
            },
            'websites.maintenance' => function($query){
                $query->whereYear('expiry_date', Carbon::now()->year)
                ->whereMonth('expiry_date', Carbon::now()->month)
                ->where('expiry_date', '>=', Carbon::today());
            }
        ])->get();


        // dd($clientsWithRenewals);

*/


/*
        //#############################
        //   CLIENTS: RENEWALS TODAY
        //#############################

        $clientsWithRenewals = Client::with([
            'websites.domains' => function($query){
                $query->whereDate('expiry_date', Carbon::today());
            },
            'websites.ssls' => function($query){
                $query->whereDate('expiry_date', Carbon::today());
            },
            'websites.hosting' => function($query){
                $query->whereDate('expiry_date', Carbon::today());
            },
            'websites.maintenance' => function($query){
                $query->whereDate('expiry_date', Carbon::today());
            }
        ])->get();

        // dd(count($clientsWithRenewals[1]['websites']));

die;
// dd($clientsWithRenewals);

*/



        //####################################
        //   WEBSITES: RENEWALS THIS MONTH
        //####################################

        // $websitesWithRenewals = Website::with(['domains' => function($query){
        //         $query->whereYear('expiry_date', Carbon::now()->year)
        //         ->whereMonth('expiry_date', Carbon::now()->month);
        //     },
        //     'hosting' => function($query){
        //         $query->whereYear('expiry_date', Carbon::now()->year)
        //         ->whereMonth('expiry_date', Carbon::now()->month);
        //     },
        //     'ssls' => function($query){
        //         $query->whereYear('expiry_date', Carbon::now()->year)
        //         ->whereMonth('expiry_date', Carbon::now()->month);
        //     },
        //     'maintenance' => function($query){
        //         $query->whereYear('expiry_date', Carbon::now()->year)
        //         ->whereMonth('expiry_date', Carbon::now()->month);
        //     }])->get();




       // return view('dashboard', compact('clientsWithRenewals', 'renewals', 'domains', 'hostings', 'ssls', 'maintenances'));
       // return view('dashboard', compact('clientsWithRenewals'));
    }
}