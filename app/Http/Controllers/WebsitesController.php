<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Website;
use App\Client;

class WebsitesController extends Controller
{

    public function index()
    {
        $websites = Website::latest()->with('client')->paginate(10);

        return view('websites.index', compact('websites'))->with('i', (request()->input('page', 1) - 1) * 10);
    }


    public function create()
    {
        $clients = Client::latest()->get();

        return view('websites.create', compact('clients'));
    }


    public function store(Request $request)
    {
        // dd($request->all());

        request()->validate([
            "name" => "required|max:255",
            "client_id" => "required"
        ]);

        $requestData = request()->all();

        if($request->has('have_domains') && $request->have_domains == 'on' )
            $requestData['have_domains'] = true;
        else
            $requestData['have_domains'] = false;

        if($request->has('has_hosting') && $request->has_hosting == 'on' )
            $requestData['has_hosting'] = true;
        else
            $requestData['has_hosting'] = false;

        if($request->has('has_email') && $request->has_email == 'on' )
            $requestData['has_email'] = true;
        else
            $requestData['has_email'] = false;

        if($request->has('has_amc') && $request->has_amc == 'on' )
            $requestData['has_amc'] = true;
        else
            $requestData['has_amc'] = false;

        if($request->has('has_ssl') && $request->has_ssl == 'on' )
            $requestData['has_ssl'] = true;
        else
            $requestData['has_ssl'] = false;


        $website = Website::create($requestData);

        // Session::flash('flash_message', 'Website added!');

        return redirect('websites')->with('status', 'Website added successfully.');
    }

    public function show(Website $website)
    {
        return view('websites.show', compact('website'));
    }


    public function edit(Website $website)
    {
        $clients = Client::latest()->get();

        return view('websites.edit', compact(['website', 'clients']));
    }


    public function update(Request $request, Website $website)
    {
        request()->validate([
            "name" => "required|max:255",
            "client_id" => "required"
        ]);


        $requestData = request()->all();

        if($request->has('have_domains') && $request->have_domains == 'on' )
            $requestData['have_domains'] = true;
        else
            $requestData['have_domains'] = false;

        if($request->has('has_hosting') && $request->has_hosting == 'on' )
            $requestData['has_hosting'] = true;
        else
            $requestData['has_hosting'] = false;

        if($request->has('has_email') && $request->has_email == 'on' )
            $requestData['has_email'] = true;
        else
            $requestData['has_email'] = false;

        if($request->has('has_amc') && $request->has_amc == 'on' )
            $requestData['has_amc'] = true;
        else
            $requestData['has_amc'] = false;

        if($request->has('has_ssl') && $request->has_ssl == 'on' )
            $requestData['has_ssl'] = true;
        else
            $requestData['has_ssl'] = false;


        $website->update($requestData);

        return redirect("websites")->with('status', 'Website updated successfully.');
    }


    public function destroy(Website $website)
    {
        Website::destroy($website->id);

        return redirect('websites')->with('status', 'Website deleted successfully.');
    }
}
