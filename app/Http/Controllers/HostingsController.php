<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Hosting;
use App\Website;
use App\Domain;
use Illuminate\Http\Request;

class HostingsController extends Controller
{

    public function index()
    {
        $hostings = Hosting::orderBy('expiry_date', 'asc')->paginate(10);

        return view('hostings.index', compact('hostings'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function websiteFilter(Website $website)
    {
        $domains = DB::table('domains')->select('*')->where('website_id', $website->id)->whereNOTIn('id', function($query){
            $query->select('domain_id')->from('hostings');
        })->pluck('name', 'id');

        return $domains;
    }

    public function create()
    {
        $websites = DB::table('websites')->select('*')->whereIn('id', function($query){
            $query->select('website_id')->from('domains');
        })->where('has_hosting', true)->latest()->pluck('name', 'id');

        return view('hostings.create', compact('websites'));
    }


    public function store(Request $request)
    {
        request()->validate([
            'website_id' => 'required',
            'domain_id' => 'required',
            'package_name' => 'required',
            'package_details' => 'required',
            'purchase_date' => 'required',
            'expiry_date' => 'required',
            // 'email_accounts' => 'required',
            // 'disk_space' => 'required',
            // 'bandwidth' => 'required',
            // 'nameserver_1' => 'required',
            // 'nameserver_2' => 'required',
            'control_panel_url' => 'required',
            'control_panel_username' => 'required',
            'control_panel_pass' => 'required'
        ]);

        $requestData = request()->all();

        if($request->has('email_accounts') && $request->email_accounts == 'on' )
            $requestData['email_accounts'] = true;
        else
            $requestData['email_accounts'] = false;


        $hosting = Hosting::create($requestData);

        return redirect('hostings')->with('status', 'Hosting added successfully.');
    }


    public function show(Hosting $hosting)
    {
        return view('hostings.show', compact('hosting'));
    }


    public function edit(Hosting $hosting)
    {
        $websites = DB::table('websites')->select('*')->whereIn('id', function($query){
            $query->select('website_id')->from('domains');
        })->where('has_hosting', true)->pluck('name', 'id');

        $domains = Domain::where('website_id', $hosting->website_id)->pluck('name', 'id');

        return view('hostings.edit', compact(['hosting', 'websites', 'domains']));
    }


    public function update(Request $request, Hosting $hosting)
    {
        request()->validate([
            'website_id' => 'required',
            'domain_id' => 'required',
            'package_name' => 'required',
            'package_details' => 'required',
            'purchase_date' => 'required',
            'expiry_date' => 'required',
            // 'email_accounts' => 'required',
            // 'disk_space' => 'required',
            // 'bandwidth' => 'required',
            // 'nameserver_1' => 'required',
            // 'nameserver_2' => 'required',
            'control_panel_url' => 'required',
            'control_panel_username' => 'required',
            'control_panel_pass' => 'required'
        ]);

        $requestData = request()->all();

        if($request->has('email_accounts') && $request->email_accounts == 'on' )
            $requestData['email_accounts'] = true;
        else
            $requestData['email_accounts'] = false;


        $hosting->update($requestData);

        return redirect("hostings")->with('status', 'Hosting updated successfully.');
    }


    public function destroy(Hosting $hosting)
    {

        Hosting::destroy($hosting->id);

        return redirect('hostings')->with('status', 'Hosting deleted successfully.');
    }
}