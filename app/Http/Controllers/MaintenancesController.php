<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Maintenance;
use App\Website;

class MaintenancesController extends Controller
{
    public function index()
    {
    	$maintenances = Maintenance::orderBy('expiry_date', 'asc')->paginate(10);

    	return view('maintenances.index', compact('maintenances'))->with('i', (request()->input('page', 1) - 1) * 10);
    }



    public function create()
    {

        $websites = Website::where('has_amc', true)->whereNOTIn('id', function($query){
            $query->select('website_id')->from('maintenances');
        })->latest()->get();

        // SELECT * FROM websites W WHERE NOT EXISTS(SELECT * FROM maintenances M WHERE W.id = M.website_id ) AND has_amc = true

        // $websites = DB::table('websites')->select('*')->whereNOTIn('id', function($query){
            // $query->select('website_id')->from('maintenances');
        // })->where('has_amc', true)->latest()->get();

        return view('maintenances.create', compact('websites'));
    }


    public function store(Request $request)
    {
        $this->validate(request(), [
            'website_id' => 'required',
            'details' => 'required',
            'start_date' => 'required',
            'expiry_date' => 'required',
            'admin_url' => 'required',
            'admin_username' => 'required',
            'admin_pass' => 'required'
        ]);

        $maintenance = Maintenance::create(request()->all());

        return redirect('/maintenances')->with('status', 'Maintenance added successfully.');
    }



    public function show(Maintenance $maintenance)
    {
        return view('maintenances.show', compact('maintenance'));
    }



    public function edit(Maintenance $maintenance)
    {

        $websites = Website::where('has_amc', true)->whereNOTIn('id', function($query){
            $query->select('website_id')->from('maintenances');
        })->latest()->get();

    	return view('maintenances.edit', compact(['websites', 'maintenance']));
    }



    public function update(Request $request, Maintenance $maintenance)
    {
        $this->validate(request(), [
            'website_id' => 'required',
            'details' => 'required',
            'start_date' => 'required',
            'expiry_date' => 'required',
            'admin_url' => 'required',
            'admin_username' => 'required',
            'admin_pass' => 'required'
        ]);

        $maintenance->update(request()->all());

        return redirect("maintenances")->with('status', 'Maintenance updated successfully.');
    }


    public function destroy(Maintenance $maintenance)
    {
        Maintenance::destroy($maintenance->id);

        return redirect('maintenances')->with('status', 'Maintenance deleted successfully.');
    }

}