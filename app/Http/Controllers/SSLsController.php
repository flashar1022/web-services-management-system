<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\SSL;
use App\Website;
use App\Domain;
use Illuminate\Http\Request;

class SSLsController extends Controller
{

    public function index()
    {
        $ssls = SSL::orderBy('expiry_date', 'asc')->paginate(10);

        return view('ssls.index', compact('ssls'))->with('i', (request()->input('page', 1) - 1) * 10);
    }


    public function websiteFilter(Website $website)
    {
        $domains = DB::table('domains')->select('*')->where('website_id', $website->id)->whereNOTIn('id', function($query){
            $query->select('domain_id')->from('ssls');
        })->pluck('name', 'id');

        return $domains;
    }

    public function create()
    {
        $websites = DB::table('websites')->select('*')->whereIn('id', function($query){
            $query->select('website_id')->from('domains');
        })->where('has_ssl', true)->latest()->pluck('name', 'id');

        return view('ssls.create', compact('websites'));
    }


    public function store(Request $request)
    {
        $this->validate(request(), [
            'website_id' => 'required',
            'domain_id' => 'required',
            'provider_name' => 'required',
            'details' => 'required',
            'purchase_date' => 'required',
            'expiry_date' => 'required',
            'control_panel_url' => 'required',
            'control_panel_username' => 'required',
            'control_panel_pass' => 'required'
        ]);

        $ssl = SSL::create(request()->all());

        return redirect('ssls')->with('status', 'SSL added successfully.');
    }


    public function show(SSL $ssl)
    {
        return view('ssls.show', compact('ssl'));
    }


    public function edit(SSL $ssl)
    {
        $websites = DB::table('websites')->select('*')->whereIn('id', function($query){
            $query->select('website_id')->from('domains');
        })->where('has_ssl', true)->latest()->pluck('name', 'id');

        $domains = Domain::where('website_id', $ssl->website_id)->pluck('name', 'id');

        return view('ssls.edit', compact('ssl', 'websites', 'domains'));
    }


    public function update(Request $request, SSL $ssl)
    {
        $this->validate(request(), [
            'website_id' => 'required',
            'domain_id' => 'required',
            'provider_name' => 'required',
            'details' => 'required',
            'purchase_date' => 'required',
            'expiry_date' => 'required',
            'control_panel_url' => 'required',
            'control_panel_username' => 'required',
            'control_panel_pass' => 'required'
        ]);

        $ssl->update(request()->all());

        return redirect("ssls")->with('status', 'SSL updated successfully.');
    }


    public function destroy(SSL $ssl)
    {
        SSL::destroy($ssl->id);

        return redirect('ssls')->with('status', 'SSL deleted successfully.');
    }
}