<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Website;
use Illuminate\Http\Request;

class DomainsController extends Controller
{

    public function index()
    {
        $domains = Domain::orderBy('expiry_date', 'asc')->paginate(10);

        return view('domains.index', compact('domains'))->with('i', (request()->input('page', 1) - 1) * 10);
    }

    public function create()
    {
        $websites = Website::where('have_domains', true)->latest()->get();

        return view('domains.create', compact('websites'));
    }


    public function store(Request $request)
    {

        $this->validate(request(), [
            'website_id' => 'required',
            'name' => 'required',
            'purchase_date' => 'required',
            'expiry_date' => 'required',
            'registrar_name' => 'required',
            'control_panel_url' => 'required',
            'control_panel_username' => 'required',
            'control_panel_pass' => 'required'
        ]);

        $domain = Domain::create(request()->all());

        return redirect('domains')->with('status', 'Domain added successfully.');

    }


    public function show(Domain $domain)
    {
        return view('domains.show', compact('domain'));
    }


    public function edit(Domain $domain)
    {
        $websites = Website::where('have_domains', true)->latest()->get();

        return view('domains.edit', compact(['domain', 'websites']));
    }


    public function update(Request $request, Domain $domain)
    {
        $this->validate(request(), [
            'website_id' => 'required',
            'name' => 'required',
            'purchase_date' => 'required',
            'expiry_date' => 'required',
            'registrar_name' => 'required',
            'control_panel_url' => 'required',
            'control_panel_username' => 'required',
            'control_panel_pass' => 'required'
        ]);

        $domain->update(request()->all());

        return redirect("domains")->with('status', 'Domain updated successfully.');

    }

    public function destroy(Domain $domain)
    {

        Domain::destroy($domain->id);

        return redirect('domains')->with('status', 'Domain deleted successfully.');
    }
}