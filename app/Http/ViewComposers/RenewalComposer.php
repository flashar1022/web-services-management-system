<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Carbon\Carbon;
use App\Domain;
use App\Hosting;
use App\Maintenance;
use App\SSL;
use App\Website;
use App\Client;

class RenewalComposer
{
    public $movieList = [];
    /**
     * Create a movie composer.
     *
     * @return void
     */
    public function __construct()
    {

        // DOMAINS EXPIRING THIS MONTH
        // $domains = Domain::whereYear('expiry_date', Carbon::now()->year)->whereMonth('expiry_date', Carbon::now()->month)->get();

        $domains = Domain::domainRenewalsThisMonth();

        $weekStart = Carbon::now()->startOfWeek()->format('Y-m-d');
        $weekEnd = Carbon::now()->endOfWeek()->format('Y-m-d');

        $renewals =[];
        $renewals[1]['thisMonth'] = count($domains);
        $renewals[1]['thisWeek'] = 0;
        $renewals[1]['today'] = 0;

        $notifications= [];

        foreach ($domains as $domain) {
            if ($domain->expiry_date >= $weekStart && $domain->expiry_date <= $weekEnd) {
                $renewals[1]['thisWeek']++;
            }
            if (Carbon::createFromFormat('Y-m-d', $domain->expiry_date)->isToday()) {
                $renewals[1]['today']++;
                $notifications['DOMAINS'][]= $domain;
            }
        }


        $hostings = Hosting::hostingRenewalsThisMonth();

        $renewals[3]['thisMonth'] = count($hostings);
        $renewals[3]['thisWeek'] = 0;
        $renewals[3]['today'] = 0;

        foreach ($hostings as $hosting) {
            if ($hosting->expiry_date >= $weekStart && $hosting->expiry_date <= $weekEnd) {
                $renewals[3]['thisWeek']++;
            }
            if (Carbon::createFromFormat('Y-m-d', $hosting->expiry_date)->isToday()) {
                $renewals[3]['today']++;
                $notifications['HOSTINGS'][]= $hosting;
            }
        }


        $maintenances = Maintenance::maintenanceRenewalsThisMonth();

        $renewals[4]['thisMonth'] = count($maintenances);
        $renewals[4]['thisWeek'] = 0;
        $renewals[4]['today'] = 0;

        foreach ($maintenances as $maintenance) {
            if ($maintenance->expiry_date >= $weekStart && $maintenance->expiry_date <= $weekEnd) {
                $renewals[4]['thisWeek']++;
            }
            if (Carbon::createFromFormat('Y-m-d', $maintenance->expiry_date)->isToday()) {
                $renewals[4]['today']++;
                $notifications['AMCS'][]= $maintenance;
            }
        }

        $ssls = SSL::sslRenewalsThisMonth();

        $renewals[2]['thisMonth'] = count($ssls);
        $renewals[2]['thisWeek'] = 0;
        $renewals[2]['today'] = 0;

        foreach ($ssls as $ssl) {
            if ($ssl->expiry_date >= $weekStart && $ssl->expiry_date <= $weekEnd) {
                $renewals[2]['thisWeek']++;
            }
            if (Carbon::createFromFormat('Y-m-d', $ssl->expiry_date)->isToday()) {
                $renewals[2]['today']++;
                $notifications['SSLS'][]= $ssl;
            }
        }

        // $this->movieList = [
        //     'Shawshank redemption',
        //     'Forrest Gump',
        //     'The Matrix',
        //     'Pirates of the Carribean',
        //     'Back to the future',
        // ];

        $this->renewals = $renewals;
        $this->notifications = $notifications;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with([
            'renewals' => $this->renewals,
            'notifications' => $this->notifications
        ]);
    }
}