<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Maintenance extends Model
{
    protected $fillable = [
		'website_id',
		'details',
		'start_date',
		'expiry_date',
		'admin_url',
		'admin_username',
		'admin_pass'
    ];

    public function website()
    {
    	return $this->belongsTo(Website::class);
    }

    public static function maintenanceRenewalsThisMonth()
    {
		return static::whereYear('expiry_date', Carbon::now()->year)
        ->whereMonth('expiry_date', Carbon::now()->month)
        ->where('expiry_date', '>=', Carbon::today())
        ->get();
    }
}