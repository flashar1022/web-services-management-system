<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Hosting extends Model
{
    protected $fillable = [
        'website_id',
        'domain_id',
        'package_name',
        'package_details',
        'purchase_date',
        'expiry_date',
        'email_accounts',
        'disk_space',
        'bandwidth',
        'nameserver_1',
        'nameserver_2',
        'control_panel_url',
        'control_panel_username',
        'control_panel_pass'
    ];

    public function website()
    {
        return $this->belongsTo(Website::class);
    }

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public static function hostingRenewalsThisMonth()
    {
        return static::whereYear('expiry_date', Carbon::now()->year)
        ->whereMonth('expiry_date', Carbon::now()->month)
        ->where('expiry_date', '>=', Carbon::today())
        ->get();
    }

}
