<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Website extends Model
{

	protected $fillable = ['client_id', 'name', 'website_type', 'have_domains', 'has_hosting', 'has_email', 'has_amc', 'has_ssl'];


	public function client()
	{
		return $this->belongsTo(Client::class);
	}

	public function domains()
	{
		return $this->hasMany(Domain::class);
	}

	public function hosting()
	{
		return $this->hasOne(Hosting::class);
	}

	public function maintenance()
	{
		return $this->hasOne(Maintenance::class);
	}

    public function ssls()
    {
        return $this->hasMany(SSL::class);
    }
}
