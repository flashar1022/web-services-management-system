<?php

namespace App\Providers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;

class DashboardServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(
            ['layouts.dash-wrap', 'dashboard'],
            'App\Http\ViewComposers\RenewalComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if (\Schema::hasTable('settings')) {
            $setting = DB::table('settings')->first();

            if($setting){
                config([
                    'website_title' => $setting->title,
                    'website_description' => $setting->description,
                    'logo' => $setting->logo,
                    'company_name' => $setting->company_name,
                    'company_url' => $setting->company_url,
                    'company_email' => $setting->company_email,
                    'auto_reminders' => $setting->auto_reminders,
                    // 'mail_driver' => $setting->mail_driver,
                    // 'mail_host' => $setting->mail_host,
                    // 'mail_port' => $setting->mail_port,
                    // 'mail_username' => $setting->mail_username,
                    // 'mail_password' => $setting->mail_password,
                    // 'mail_encryption' => $setting->mail_encryption,
                    // 'mail_from_name' => $setting->mail_from_name,
                    // 'mail_from_address' => $setting->mail_from_address,
                ]);                
            }
        }
    }
}
