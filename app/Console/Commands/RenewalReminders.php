<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Mail\DomainRenewalEmail;
use App\Mail\SSLRenewalEmail;
use App\Mail\HostingRenewalEmail;
use App\Mail\AMCRenewalEmail;
use App\Domain;
use App\SSL;
use App\Hosting;
use App\Maintenance;
use Carbon\Carbon;

class RenewalReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'renewalReminders:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send renewal reminders to Clients';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $today = Carbon::today()->setTimezone('Asia/Kolkata');

        $domains = Domain::where('expiry_date', '<=', $today->copy()->addDays(30))
            ->where('expiry_date', '>=', $today->copy()->subDays(2))
            ->get();

        $ssls = SSL::where('expiry_date', '<=', $today->copy()->addDays(30))
            ->where('expiry_date', '>=', $today->copy()->subDays(2))
            ->get();

            // dd($ssls);

        $hostings = Hosting::where('expiry_date', '<=', $today->copy()->addDays(30))
            ->where('expiry_date', '>=', $today->copy()->subDays(2))
            ->get();

        $maintenances = Maintenance::where('expiry_date', '<=', $today->copy()->addDays(30))
            ->where('expiry_date', '>=', $today->copy()->subDays(2))
            ->orderBy('expiry_date')
            ->get();


        foreach ($domains as $domain) {

            $renewal['renewal'] = false;
            $renewal['inDays'] = '';
            $expDt = Carbon::createFromFormat('Y-m-d', $domain->expiry_date);

            if ($expDt->format('Y-m-d') == $today->copy()->addDays(30)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 30 days';
            } elseif ($expDt == $today->copy()->addDays(7)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 7 days';
            } elseif ($expDt->isToday()) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring today';
            } elseif ($expDt == $today->copy()->subDays(2)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'has expired 2 days ago';
            }

            if ($renewal['renewal']) {
                Mail::to($domain->website->client->email)->queue(new DomainRenewalEmail($domain, $renewal['inDays']));
                $this->info($domain->name . ' renewal in/on ' . $renewal['inDays']);
            }

        }

        foreach ($ssls as $ssl) {

            $renewal['renewal'] = false;
            $renewal['inDays'] = '';
            $expDt = Carbon::createFromFormat('Y-m-d', $ssl->expiry_date);

            if ($expDt->format('Y-m-d') == $today->copy()->addDays(30)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 30 days';
            } elseif ($expDt == $today->copy()->addDays(7)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 7 days';
            } elseif ($expDt->isToday()) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring today';
            } elseif ($expDt == $today->copy()->subDays(2)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'has expired 2 days ago';
            }

            if ($renewal['renewal']) {
                Mail::to($ssl->website->client->name)->queue(new SSLRenewalEmail($ssl, $renewal['inDays']));
                $this->info($ssl->domain->name . ' renewal in/on ' . $renewal['inDays']);
            }

        }


        foreach ($hostings as $hosting) {

            $renewal['renewal'] = false;
            $renewal['inDays'] = '';
            $expDt = Carbon::createFromFormat('Y-m-d', $hosting->expiry_date);

            if ($expDt->format('Y-m-d') == $today->copy()->addDays(30)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 30 days';
            } elseif ($expDt == $today->copy()->addDays(7)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 7 days';
            } elseif ($expDt->isToday()) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring today';
            } elseif ($expDt == $today->copy()->subDays(2)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'has expired 2 days ago';
            }

            if ($renewal['renewal']) {
                Mail::to($hosting->website->client->name)->queue(new HostingRenewalEmail($hosting, $renewal['inDays']));
                $this->info('hosting ' . $hosting->domain->name . ' renewal in/on ' . $renewal['inDays']);
            }

        }



// dd(Carbon::today()->addDays(7)->format('Y-m-d'));

        foreach ($maintenances as $maintenance) {

            $this->info($maintenance->expiry_date . ' ' . $today->copy()->subDays(2)->format('Y-m-d') . "\n");

            $renewal['renewal'] = false;
            $renewal['inDays'] = '';
            $expDt = Carbon::createFromFormat('Y-m-d', $maintenance->expiry_date);

            // dd($today->copy()->subDays(2)->format('Y-m-d'));

            if ($expDt->format('Y-m-d') == $today->copy()->addDays(30)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 30 days';
            } elseif ($expDt == $today->copy()->addDays(7)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring in 7 days';
            } elseif ($expDt->isToday()) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'is expiring today';
            } elseif ($expDt == $today->copy()->subDays(2)->format('Y-m-d')) {
                $renewal['renewal'] = true;
                $renewal['inDays'] = 'has expired 2 days ago';
            }

            if ($renewal['renewal']) {
                Mail::to($maintenance->website->client->name)->queue(new AMCRenewalEmail($maintenance, $renewal['inDays']));
                $this->info('maintenance ' . $maintenance->id . ') ' . $maintenance->expiry_date . ' renewal in/on ' . $renewal['inDays']);
            }

        }




/*
        Mail::send('emails.renewals', ['title' => 'Shaktiman', 'content' => 'Pt.Gangadhar Shastri'], function ($message)
        {

            // $message->from('me@gmail.com', 'Amol Naik');
            $message->to('amolbnaik@yahoo.co.in')->subject('WEBPRO: Renewal Reminder-Shaktiman');

        });
*/


        // Mail::to('amolbnaik@yahoo.co.in')->bcc('abn.9122@gmail.com')->queue(new RenewalEmail($data));
        //     $this->info('sending email');

        // return response()->json(['message' => 'Request completed']);
        // dd($clientsWithRenewals->toArray());

    }
}
