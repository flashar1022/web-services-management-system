@extends('layouts.dash-wrap')


@section('content')


	<!-- <div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Users</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/users" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/users/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div> -->

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Change password</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/users/{{ $user->id }}" method="POST">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Name:</label>
								<div class="col-6">
									<input type="text" class="form-control" id="user_name" name="name" placeholder="Name" value="{{ $user->name }}" disabled>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Email address:</label>
								<div class="col-6">
									<input type="email" class="form-control" id="user_email" name="email" placeholder="Email" value="{{ $user->email }}" disabled="">
								</div>
							</div>

						    <div class="form-group m-form__group row">
						        <label class="col-3 col-form-label" for="">Password</label>
						        <div class="col-6">
						        	<input type="text" class="form-control" id="user_pass" name="password" placeholder="Password" required="" value="" autocomplete="off">
						        </div>
						    </div>

						    <div class="form-group m-form__group row">
						        <label class="col-3 col-form-label" for="">Confirm Password</label>
						        <div class="col-6">
						        	<input type="text" class="form-control" id="user_pass_confirmation" name="password_confirmation" placeholder="Confirm Password" required="" value="" autocomplete="off">
						        </div>
						    </div>

						</div>

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>
					</form>
				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>


@endsection