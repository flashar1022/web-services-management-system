@extends('layouts.dash-wrap')

@section('content')

			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Users</h3>
						<div class="d-inline-block">
							<a href="/users" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
							<a href="/users/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
						</div>
					</div>
				</div>
			</div>

			<div class="m-content">
				<div class="row">

					<div class="col-lg-12">

	@if (session('status'))
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
    	    {{ session('status') }}
    	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	</div>
	@endif

						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Users' List
										</h3>
									</div>
								</div>
							</div>

							<div class="m-portlet__body">

								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Name</th>
											<th>Email</th>
											<th>Actions</th>
										</tr>
									</thead>

									<tbody>

										@if (count($users))

											@foreach ($users as $user)
											<tr>
												<td>{{ ++$i }}</td>
												<td>{{ $user->name }}</td>
												<td>{{ $user->email }}</td>
												<td>
													{{-- <a href="/users/{{ $user->id }}"><i class="fa fa-eye"></i></a> --}}
													<a href="/users/{{ $user->id }}/edit">&nbsp;<i class="fa fa-pencil"></i>&nbsp;</a>
													<form action="/users/{{ $user->id }}" method="POST" class="d-inline" id="delete-user-{{ $user->id }}">
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<a href="#" onclick="deleteUser({{$user->id}}, '{{ $user->name }}')"><i class="fa fa-trash"></i></a>
													</form>
												</td>
											</tr>
											@endforeach

											<script>
												function deleteUser(deleteID, userName) {
													console.log('clicked : ' + deleteID + ' : ' + userName);
													r = confirm('Are you sure you want to delete '+ userName +'?');
													if (r == true) {
														document.getElementById('delete-user-' + deleteID).submit();
													} else {
														console.log('cancelled');
													}
												}
											</script>

										@else

											<tr>
												<td colspan="6"><div class="alert alert-danger">{{ 'No user records found' }}</div></td>
											</tr>

										@endif

									</tbody>
								</table>

								{!! $users->render() !!}

							</div>
						</div> {{-- m-portlet END --}}

					</div>
				</div>
			</div>

@endsection