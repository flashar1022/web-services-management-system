@extends('layouts.dash-wrap')


@section('content')


	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title d-inline-block">Settings</h3>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">

	@if (session('status'))
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
    	    {{ session('status') }}
    	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	</div>
	@endif
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Update settings</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/settings" method="POST" enctype="multipart/form-data">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Website title</label>
								<div class="col-6">
									<input type="text" class="form-control" id="title" name="title" placeholder="Website title" required="" value="{{ $setting->title }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Website Description</label>
								<div class="col-6">
									<textarea class="form-control" id="description" name="description" placeholder="Website description" required="">{{ $setting->description }}</textarea>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Logo</label>
								<div class="col-6">
									<input type="file" class="form-control" id="logo" name="logo" placeholder="Upload logo" required="" value="{{ $setting->logo }}" accept="image/*">
									<span class="m-form__help">Max logo dimension: <span class="text-danger">195 X 70px</span> <br> Max logo file size: <span class="text-danger">2mb</span></span>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Company Name:</label>
								<div class="col-6">
									<input type="text" class="form-control" id="company_name" name="company_name" placeholder="Company Name" required="" value="{{ $setting->company_name }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Company Website</label>
								<div class="col-6">
									<input type="url" class="form-control" id="company_url" name="company_url" placeholder="e.g. http://example.com" required="" value="{{ $setting->company_url }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Company Email</label>
								<div class="col-6">
									<input type="email" class="form-control" id="company_email" name="company_email" placeholder="Company Email" required="" value="{{ $setting->company_email }}">
									<span class="m-form__help">Will be used to send renewal emails</span>

								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Auto Reminders</label>
								<div class="col-6">
									<div class="m-radio-inline">
										<label class="m-radio">
											<input type="radio" name="auto_reminders" value="1" {{ $setting->auto_reminders == 1 ? 'checked' : ''}} >
											On
											<span></span>
										</label>
										<label class="m-radio">
											<input type="radio" name="auto_reminders" value="0" {{ $setting->auto_reminders == 0 ? 'checked' : ''}} >
											Off
											<span></span>
										</label>
									</div>
									<span class="m-form__help">Send reminder emails automatically before a month, a week before expiry date, on the day of expiration and 2 days after the expiry.</span>
								</div>
							</div>

						</div>

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>
					</form>
				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>


@endsection