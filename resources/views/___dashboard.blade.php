@extends('layouts.dash-wrap')


@section('content')
<?php use Carbon\Carbon; ?>
                    <!-- BEGIN: Subheader -->
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    Dashboard
                                </h3>
                            </div>
                            <div>
                                <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                                <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>
                                <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-angle-down"></i>
                                </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- END: Subheader -->

                    <div class="m-content">

                        <div class="row">
                            <div class="col-xl-12">
                                @foreach ($clientsWithRenewals as $client)

                                @if(count($client['websites']))

                                    <?php $itrs = count($client['websites']); $i=1; ?>

                                    @foreach ($client->websites as $key => $website)

                                    <?php ++$key; ?>

{{--                                     {{ 'websites ' . $itrs . ' key ' . $key . ' noOfWebRenewals ' . $i }}<br>
 --}}
                                    @if(count($website['domains']) || count($website['ssls']) || $website['hosting'] || $website['maintenance'])

                                        <!--
                                            output opening portlet HTML on first website iteration
                                        -->
                                        @if($key == 1)

                                        <div class="m-portlet">
                                            <div class="m-portlet__head">
                                                <div class="m-portlet__head-caption">
                                                    <div class="m-portlet__head-title">
                                                        <h3 class="m-portlet__head-text"><strong>{{ $client->name }}</strong></h3>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-portlet__body">
                                                <div class="table-responsive">
                                                    <table class="table">
                                                        <thead>
                                                            <tr>
                                                                <th class="m-widget11__label">
                                                                    #
                                                                </th>
                                                                <th class="m-widget11__app">
                                                                    Website
                                                                </th>
                                                                <th class="m-widget11__sales">
                                                                    Domains
                                                                </th>
                                                                <th class="m-widget11__price">
                                                                    Hosting
                                                                </th>
                                                                <th class="m-widget11__price">
                                                                    Maintenance
                                                                </th>
                                                                <th class="m-widget11__total m--align-right">
                                                                    SSLs
                                                                </th>
                                                            </tr>
                                                        </thead>

                                                        <tbody>
                                        @endif

                                                        <tr>
                                                            <td>{{ $i }}</td>
                                                            <td>
                                                                <span class="m-widget11__title">
                                                                    {{ $website->name }}
                                                                </span>
                                                                {{-- <span class="m-widget11__sub">
                                                                    Vertex To By Again
                                                                </span> --}}
                                                            </td>

                                                        @if (count($website->domains))
                                                            <td>
                                                                @foreach ($website->domains as $domain)
                                                                <a href="/domains/{{ $domain->id }}">{{ $domain->name }}</a>:&nbsp;
                                                                <?php $expDt = Carbon::createFromFormat('Y-m-d', $domain->expiry_date); ?>

                                                                {{-- {{ $expDt . ' > ' . Carbon::now() . ' > ' . Carbon::now()->addWeek() }} --}}

                                                                @if ($expDt->isToday())
                                                                    <span class="m-badge m-badge--danger m-badge--wide">Today</span>
                                                                @elseif($expDt->between(Carbon::now(), Carbon::now()->addWeek()))
                                                                    <span class="m-badge m-badge--warning m-badge--wide">in {{ $expDt->diffForHumans(null, true) }}</span>
                                                                @else
                                                                    <span class="m-badge m-badge--info m-badge--wide">This Week</span>
                                                                @endif

                                                                <br/>
                                                                @endforeach
                                                            </td>
                                                        @else
                                                            <td class="text-center">&mdash;</td>
                                                        @endif

                                                            <td class="text-center">
                                                        @if($website->hosting)
                                                                @php
                                                                    $expDt = Carbon::createFromFormat('Y-m-d', $website->hosting['expiry_date']);
                                                                @endphp

                                                                @if ($expDt->isToday())
                                                                    <span class="m-badge m-badge--danger m-badge--wide">Today</span>
                                                                @elseif($expDt->between(Carbon::now(), Carbon::now()->addWeek()))
                                                                    <span class="m-badge m-badge--warning m-badge--wide">in {{ $expDt->diffForHumans(null, true) }}</span>
                                                                @else
                                                                    <span class="m-badge m-badge--info m-badge--wide">This Week</span>
                                                                @endif
                                                            </td>
                                                        @else
                                                                &mdash;
                                                        @endif
                                                            </td>

                                                            <td class="text-center">
                                                            @if($website->maintenance)

                                                                @php
                                                                    $expDt = Carbon::createFromFormat('Y-m-d', $website->maintenance['expiry_date']);
                                                                @endphp
{{$expDt}}
                                                                @if ($expDt->isToday())
                                                                    <span class="m-badge m-badge--danger m-badge--wide">Today</span>
                                                                @elseif($expDt->between(Carbon::now(), Carbon::now()->addWeek()))
                                                                    <span class="m-badge m-badge--warning m-badge--wide">in {{ $expDt->diffForHumans(null, true) }}</span>
                                                                @else
                                                                    <span class="m-badge m-badge--info m-badge--wide">This Week</span>
                                                                @endif
                                                            </td>
                                                            @else
                                                                &mdash;
                                                            @endif
                                                            </td>

                                                            @if (count($website->ssls))
                                                            <td>
                                                                @foreach ($website->ssls as $key => $ssl)
                                                                <a href="/ssls/{{ $ssl->id }}">SSL {{ ++$key }}</a>:&nbsp;
                                                                <?php $expDt = Carbon::createFromFormat('Y-m-d', $ssl->expiry_date); ?>

                                                                @if ($expDt->isToday())
                                                                    <span class="m-badge m-badge--danger m-badge--wide">Today</span>
                                                                @else
                                                                {{ $expDt->diffForHumans(null, true) }}
                                                                @endif

                                                                <br/>
                                                                @endforeach
                                                            </td>
                                                            @else
                                                            <td class="text-center">&mdash;</td>
                                                            @endif
                                                        </tr>


                                        <?php $i++; ?>


                                    @endif


                                    @endforeach

                                    <!--
                                        output closing portlet/table tags only if the last website iteration is complete
                                    -->
                                    @if ($key == $itrs)

                                                    </tbody><!--end::Tbody-->
                                                </table><!--end::Table-->
                                            </div><!-- end:: table-responsive -->
                                        </div><!-- end:: m-portlet__body -->
                                    </div><!-- end:: m-portlet -->
                                    @endif

                                @endif

                                @endforeach
                            </div>
                        </div>
                    </div>
@stop