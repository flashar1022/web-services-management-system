<!DOCTYPE html>
<html>
<head>
	<title>{{ $maintenance->website->name }} maintenance renewal reminder | {{ $maintenance->website->name . ' maintenance package ' . $expiring }}</title>
	<style type="text/css">
		body {
			font-family: sans-serif;
		}
		table {
			width: 90%;
		}
		table th,
		table td {
			border: 1px solid black;
		}
	</style>
</head>
<body>
<p>Dear <strong>{{ $maintenance->website->client->name }}</strong>,<br />
	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Maintenance package for your Website <strong><a href="{{ $maintenance->website->name }}">{{ $maintenance->website->name }}</a> {{ $expiring }}</strong> on <strong>{{ $maintenance->expiry_date }}</strong> and this is a reminder mail for the renewal of the same. Kindly renew your Maintenance package. </p>

<table border="0" cellpadding="5" cellspacing="0">
	<tr>
		<th>#</th>
		<th>Website Name</th>
		<th>Maintenance Expiry Date</th>
	</tr>
	<tr>
		<td>1</td>
		<td>{{ $maintenance->website->name }}</td>
		<td>{{ $maintenance->expiry_date }}</td>
	</tr>
</table>

<p>
Thanks &amp; Regards,<br />
WebPro Support Team<br />
<a href="http://www.test.com">www.test.com</a><br />
Phone: <a href="tel:9890098900">+91-422-5522255222</a><br />
Mobile: <a href="tel:9890098900">+91-55555555555</a><br />
Contact Mail : <a href="mailto:support@test.com">support@test.com</a><br />
</p>


</body>
</html>