@extends('layouts.dash-wrap')


@section('content')
	{{-- {{dd($client)}} --}}

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Client Details</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/clients" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/clients/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">

					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">{{ $client->name }}</h3>
							</div>
						</div>
					</div>

					<div class="m-portlet__body">

						<dl class="row">
							<dt class="col-3 text-right">Name</dt>
							<dd class="col-9">{{ $client->name }}</dd>

							<dt class="col-3 text-right">Company Name</dt>
							<dd class="col-9">{{ $client->company_name }}</dd>

							<dt class="col-3 text-right">Job Title</dt>
							<dd class="col-9">{{ $client->job_title }}</dd>

							<dt class="col-3 text-right">Phone</dt>
							<dd class="col-9">{{ $client->phone }}</dd>

							<dt class="col-3 text-right">Mobile</dt>
							<dd class="col-9">{{ $client->mobile }}</dd>

							<dt class="col-3 text-right">Email</dt>
							<dd class="col-9">{{ $client->email }}</dd>

							<dt class="col-3 text-right">Address</dt>
							<dd class="col-9">{{ $client->address }}</dd>

							<dt class="col-3 text-right">Client Note</dt>
							<dd class="col-9">{{ $client->client_note }}</dd>
						</dl>
					</div><!-- div.m-portlet__body END-->

					<div class="m-portlet__foot ">

						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<a href="/clients/{{  $client->id }}/edit" class="btn btn-warning">Edit</a>
								&nbsp;
								<form action="/clients/{{  $client->id }}" method="POST" class="m-form m-form--label-align-right d-inline" id="delete-client-{{ $client->id }}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<a href="#" onclick="deleteClient()" class="btn btn-danger">Delete</a>
								</form>
								<script>
									function deleteClient() {
										r = confirm('Are you sure you want to delete \'{{ $client->name }}\'?');
										if (r == true) {
											document.getElementById('delete-client-{{ $client->id }}').submit();
										} else {
											console.log('cancelled');
										}
									}
								</script>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

@stop