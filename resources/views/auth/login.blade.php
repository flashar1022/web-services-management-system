@extends('layouts.auth-wrap')

@section('content')

    <div class="m-login__signin">

        <div class="m-login__head">
            <h3 class="m-login__title">Sign In To Admin</h3>
        </div>

        <form class="m-login__form m-form" method="POST" action="{{ route('login') }}">

            {{ csrf_field() }}

            <div class="form-group m-form__group{{ $errors->has('email') ? ' has-error' : '' }}">

                <input id="email" type="email" class="form-control m-input" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>

                @if ($errors->has('email'))
                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                @endif

            </div>


            <div class="form-group m-form__group">

                <input id="password" type="password" class="form-control m-input m-login__form-input--last" name="password" placeholder="Password" required>

                @if ($errors->has('password'))
                    <strong class="text-danger">{{ $errors->first('password') }}</strong>
                @endif

            </div>


            <div class="row m-login__form-sub">

                <div class="col m--align-left m-login__form-left">
                    <label class="m-checkbox  m-checkbox--light">
                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        <span></span>
                    </label>
                </div>

                <div class="col m--align-right m-login__form-right">
                    {{-- <a href="{{ route('password.request') }}" id="m_login_forget_password" class="m-link"> --}}
                    <a href="{{ route('password.request') }}" id="" class="m-link">Forgot Password?</a>
                </div>

            </div>


            <div class="m-login__form-action">
                {{-- <button id="m_login_signin_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary"> --}}

                <button type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn m-login__btn--primary">Sign In</button>
            </div>

        </form>
    </div>
@endsection
