@extends('layouts.auth-wrap')

@section('content')

    <div class="m-login__signup" style="display: block;">
        <div class="m-login__head">
            <h3 class="m-login__title">Sign Up</h3>
            <div class="m-login__desc">Enter your details to create your account:</div>
        </div>
        <form class="m-login__form m-form" method="POST" action="{{ route('register') }}">

            {{ csrf_field() }}

            <div class="form-group m-form__group{{ $errors->has('name') ? ' has-error' : '' }}">

                <input id="name" type="text" class="form-control m-input" name="name" value="{{ old('name') }}" placeholder="Name" name="fullname" required autofocus>

                @if ($errors->has('name'))
                    <strong class="text-danger">{{ $errors->first('name') }}</strong>
                @endif

            </div>

            <div class="form-group m-form__group{{ $errors->has('email') ? ' has-error' : '' }}">

                <input id="email" type="email" class="form-control m-input" name="email" value="{{ old('email') }}" placeholder="Email" required>

                @if ($errors->has('email'))
                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                @endif

            </div>

            <div class="form-group m-form__group{{ $errors->has('password') ? ' has-error' : '' }}">

                <input id="password" type="password" class="form-control m-input" placeholder="Password" name="password" required>

                @if ($errors->has('password'))
                    <span class="help-block">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif

            </div>

            <div class="form-group m-form__group">

                <input id="password-confirm" type="password" class="form-control m-input m-login__form-input--last" name="password_confirmation" placeholder="Confirm Password" required>

            </div>

            <div class="m-login__form-action">
                {{-- <button id="m_login_signup_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary"> --}}
                <button id="" type="submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary">
                    Register
                </button>
                &nbsp;&nbsp;
                <a href="{{ url('/')}}" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">
                    Cancel
                </a>
            </div>

        </form>
    </div>

{{-- <div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

@endsection
