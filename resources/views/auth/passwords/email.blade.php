@extends('layouts.auth-wrap')

@section('content')

    <div class="m-login__forget-password" style="display: block;">
        <div class="m-login__head">
            <h3 class="m-login__title">
                Forgotten Password?
            </h3>
            <div class="m-login__desc">
                Enter your email to reset your password:
            </div>
        </div>

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <form class="m-login__form m-form" method="POST" action="{{ route('password.email') }}">
            {{ csrf_field() }}


            <div class="form-group m-form__group{{ $errors->has('email') ? ' has-error' : '' }}">

                <input id="email" type="email" class="form-control m-input" name="email" placeholder="Email" value="{{ old('email') }}">

                @if ($errors->has('email'))
                    <strong class="text-danger">{{ $errors->first('email') }}</strong>
                @endif

            </div>

            <div class="m-login__form-action">

                {{-- <button id="m_login_forget_password_submit" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary" type="submit">Request</button> --}}
                
                <button id="" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn m-login__btn--primary" type="submit">Send Password Reset Link</button>
                &nbsp;&nbsp;

                {{-- <button id="m_login_forget_password_cancel" class="btn m-btn m-btn--pill m-btn--custom m-btn--air m-login__btn">Cancel</button> --}}

            </div>
        </form>
    </div>

@endsection
