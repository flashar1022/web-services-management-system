@extends('layouts.dash-wrap')

@section('content')

			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Websites</h3>
						<div class="d-inline-block">
							<a href="/websites" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
							<a href="/websites/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
						</div>
					</div>
				</div>
			</div>

			<div class="m-content">
				<div class="row">

					<div class="col-lg-12">

	@if (session('status'))
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
    	    {{ session('status') }}
    	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	</div>
	@endif

						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Websites' List
										</h3>
									</div>
								</div>
							</div>

							<div class="m-portlet__body">

								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Website Name</th>
											<th>Client Name</th>
											<th>Website Type</th>
											<th>Domains?</th>
											<th>Hosting?</th>
											<th>Emails?</th>
											<th>SSLs?</th>
											<th>AMC?</th>
											<th>Actions</th>
										</tr>
									</thead>

									<tbody>

									@if (count($websites))

										@foreach ($websites as $website)

										<tr>
											<td>{{ ++$i }}</td>
											<td>{{ $website->name }}</td>
											<td><a href="/clients/{{ $website['client']['id'] }}">{{ $website['client']['name'] }}</a></td>
											<td>{{ $website->website_type }}</td>
											<td>
												@if ($website->have_domains)
													<span class="m-badge m-badge--success m-badge--wide">Yes</span>
												@else
													<span class="m-badge m-badge--danger m-badge--wide">No</span>
												@endif
											</td>
											<td>
												@if ($website->has_hosting)
													<span class="m-badge m-badge--success m-badge--wide">Yes</span>
												@else
													<span class="m-badge m-badge--danger m-badge--wide">No</span>
												@endif
											</td>
											<td>
												@if ($website->has_email)
													<span class="m-badge m-badge--success m-badge--wide">Yes</span>
												@else
													<span class="m-badge m-badge--danger m-badge--wide">No</span>
												@endif
											</td>
											<td>
												@if ($website->has_ssl)
													<span class="m-badge m-badge--success m-badge--wide">Yes</span>
												@else
													<span class="m-badge m-badge--danger m-badge--wide">No</span>
												@endif
											</td>
											<td>
												@if ($website->has_amc)
													<span class="m-badge m-badge--success m-badge--wide">Yes</span>
												@else
													<span class="m-badge m-badge--danger m-badge--wide">No</span>
												@endif
											</td>
											<td>
												<a href="/websites/{{ $website->id }}"><i class="fa fa-eye"></i></a>
												<a href="/websites/{{ $website->id }}/edit">&nbsp;<i class="fa fa-pencil"></i>&nbsp;</a>
												<form action="/websites/{{ $website->id }}" method="POST" class="d-inline" id="delete-website-{{ $website->id }}">
													{{ csrf_field() }}
													{{ method_field('DELETE') }}
													<a href="#" onclick="deleteWebsite({{ $website->id }}, '{{ $website->name }}')"><i class="fa fa-trash"></i></a>
												</form>
											</td>
										</tr>

										@endforeach

										<script>
											function deleteWebsite(deleteID, websiteName) {
												console.log('clicked ' + deleteID + ' : ' + websiteName);
												r = confirm('Are you sure you want to delete \'' + websiteName + '\'?');
												if (r == true) {
													document.getElementById('delete-website-' + deleteID).submit();
												} else {
													console.log('cancelled');
												}
											}
										</script>

									@else
										<tr>
											<td colspan="6"><div class="alert alert-danger">{{ 'No website records found' }}</div></td>
										</tr>
									@endif

									</tbody>
								</table>

								{!! $websites->render() !!}

							</div>
						</div> {{-- m-portlet END --}}

					</div>
				</div>
			</div>

@stop