@extends('layouts.dash-wrap')

@section('content')
{{-- {{dd($website->name)}} --}}
	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Website Details</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/websites" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/websites/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">

					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">{{ $website->name }}</h3>
							</div>
						</div>
					</div>

					<div class="m-portlet__body">

						<dl class="row">
							<dt class="col-3 text-right">Website Name</dt>
							<dd class="col-9">{{ $website->name }}</dd>

							<dt class="col-3 text-right">Client Name</dt>
							<dd class="col-9">{{ $website->client->name }}</dd>

							<dt class="col-3 text-right">Website Type</dt>
							<dd class="col-9">{{ $website->website_type }}</dd>

							<dt class="col-3 text-right">Have Domains?</dt>
							<dd class="col-9">
								@if ($website->have_domains)
								<span class="m-badge m-badge--success m-badge--wide">Yes</span>
								@else
									<span class="m-badge m-badge--danger m-badge--wide">No</span>
								@endif
							</dd>

							<dt class="col-3 text-right">Have SSLs?</dt>
							<dd class="col-9">
								@if ($website->has_ssl)
								<span class="m-badge m-badge--success m-badge--wide">Yes</span>
								@else
									<span class="m-badge m-badge--danger m-badge--wide">No</span>
								@endif
							</dd>

							<dt class="col-3 text-right">Has Hosting?</dt>
							<dd class="col-9">
								@if ($website->has_hosting)
								<span class="m-badge m-badge--success m-badge--wide">Yes</span>
								@else
									<span class="m-badge m-badge--danger m-badge--wide">No</span>
								@endif
							</dd>

							<dt class="col-3 text-right">Has Email hosting?</dt>
							<dd class="col-9">
								@if ($website->has_email)
								<span class="m-badge m-badge--success m-badge--wide">Yes</span>
								@else
									<span class="m-badge m-badge--danger m-badge--wide">No</span>
								@endif
							</dd>

							<dt class="col-3 text-right">Has AMC?</dt>
							<dd class="col-9">
								@if ($website->has_amc)
								<span class="m-badge m-badge--success m-badge--wide">Yes</span>
								@else
									<span class="m-badge m-badge--danger m-badge--wide">No</span>
								@endif
							</dd>
						</dl>

					</div><!-- div.m-portlet__body END-->

					<div class="m-portlet__foot">
						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<a href="/websites/{{  $website->id }}/edit" class="btn btn-warning">Edit</a>
								&nbsp;
								<form action="/websites/{{  $website->id }}" method="POST" class="m-form m-form--label-align-right d-inline" id="delete-website-{{ $website->id }}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<a href="#" onclick="deleteWebsite()" class="btn btn-danger">Delete</a>
								</form>
								<script>
									function deleteWebsite() {
										r = confirm('Are you sure you want to delete \'{{ $website->name }}\'?');
										if (r == true) {
											document.getElementById('delete-website-{{ $website->id }}').submit();
										} else {
											console.log('cancelled');
										}
									}
								</script>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@stop