@extends('layouts.dash-wrap')

@section('content')

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Websites</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/websites" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/websites/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Add Website</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/websites" method="POST">

						{{ csrf_field() }}

						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Website Name</label>
								<div class="col-6">
									<input type="text" class="form-control" id="website_name" name="name" placeholder="Name" required="">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Client Name
								</label>
								<div class="col-6">
									<select class="form-control" id="website_client_id" name="client_id">

										@foreach ($clients as $client)
											<option value="{{ $client->id }}">{{ $client->name }}</option>
										@endforeach

									</select>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">Website Type</label>
								<div class="col-6">
									<div class="m-radio-inline">
										<label class="m-radio">
											<input type="radio" name="website_type" value="Static" checked>
											Static
											<span></span>
										</label>
										<label class="m-radio">
											<input type="radio" name="website_type" value="CMS">
											CMS
											<span></span>
										</label>
										<label class="m-radio">
											<input type="radio" name="website_type" value="Custom">
											Custom
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-4 col-form-label">
									<strong>Select other services provided: </strong>
								</label>
								<div class="col-6">
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Domains
								</label>
								<div class="col-6">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="have_domains">
											<span></span>
										</label>
									</div>
								</div>
							</div>


							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Hosting
								</label>
								<div class="col-6">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_hosting">
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Emails
								</label>
								<div class="col-6">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_email">
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Maintenance
								</label>
								<div class="col-6">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_amc">
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									SSL
								</label>
								<div class="col-6">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_ssl">
											<span></span>
										</label>
									</div>
								</div>
							</div>
						</div><!-- m-portlet__body END -->

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-6">
										<button type="submit" class="btn btn-success">Submit</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>

					</form>

				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>
{{-- <script type="text/javascript">
	jQuery(document).ready(function() {
	    var data = [{
	        id: 0,
	        text: 'Enhancement'
	    }, {
	        id: 1,
	        text: 'Bug'
	    }, {
	        id: 2,
	        text: 'Duplicate'
	    }, {
	        id: 3,
	        text: 'Invalid'
	    }, {
	        id: 4,
	        text: 'Wontfix'
	    }];

	    $('#m_select2_5').select2({
	        placeholder: "Select a value",
	        data: data
	    });
	});
</script>
 --}}
 @stop