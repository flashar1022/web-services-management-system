@extends('layouts.dash-wrap')

@section('content')

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Websites</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/websites" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/websites/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Edit Website</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/websites/{{ $website->id }}" method="POST">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Website Name</label>
								<div class="col-6">
									<input type="text" class="form-control" id="website_name" name="name" placeholder="Name" required="" value="{{ $website->name }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">
									Client Name
								</label>
								<div class="col-6">
									<select class="form-control" id="website_client_id" name="client_id">

										@foreach ($clients as $client)

											@if ($client->id == $website->client->id )
												<option selected value="{{ $client->id }}">{{ $client->name }}</option>
											@else
												<option value="{{ $client->id }}">{{ $client->name }}</option>
											@endif

										@endforeach

									</select>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">Website Type</label>
								<div class="col-9">
									<div class="m-radio-inline">
										<label class="m-radio">
											<input type="radio" name="website_type" value="Static" {{ $website->website_type == 'Static' ? 'checked' : ''  }} >
											Static
											<span></span>
										</label>
										<label class="m-radio">
											<input type="radio" name="website_type" value="CMS" {{ $website->website_type == 'CMS' ? 'checked' : ''  }} >
											CMS
											<span></span>
										</label>
										<label class="m-radio">
											<input type="radio" name="website_type" value="Custom" {{ $website->website_type == 'Custom' ? 'checked' : ''  }} >
											Custom
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-4 col-form-label">
									<strong>Select other services provided: </strong>
								</label>
								<div class="col-6">
								</div>
							</div>


							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Domains
								</label>
								<div class="col-9">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="have_domains" {{ $website->have_domains ? 'checked' : ''}}>
											<span></span>
										</label>
									</div>
								</div>
							</div>


							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Hosting
								</label>
								<div class="col-9">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_hosting" {{ $website->has_hosting ? 'checked' : ''}}>
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Emails
								</label>
								<div class="col-9">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_email" {{ $website->has_email ? 'checked' : ''}}>
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									Maintenance
								</label>
								<div class="col-9">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_amc" {{ $website->has_amc ? 'checked' : ''}}>
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="m-form__group form-group row">
								<label class="col-3 col-form-label">
									SSL
								</label>
								<div class="col-9">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" name="has_ssl" {{ $website->has_ssl ? 'checked' : ''}}>
											<span></span>
										</label>
									</div>
								</div>
							</div>
						</div><!-- m-portlet__body END -->

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-lg-2"></div>
									<div class="col-lg-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>
					</form>
				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>
@stop