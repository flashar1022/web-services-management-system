@extends('layouts.dash-wrap')


@section('content')
<?php use Carbon\Carbon; ?>
                    <!-- BEGIN: Subheader -->
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    Dashboard
                                </h3>
                            </div>
                            <div>
                                <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                                <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>
                                <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-angle-down"></i>
                                </a>
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- END: Subheader -->

                    <div class="m-content">
{{--                         <div class="m-portlet">
                            <div class="m-portlet__body m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-xl-4">
                                        begin:: Widgets/Stats2-1
                                        <div class="m-widget1">
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col">
                                                        <h3 class="m-widget1__title">
                                                            Member Profit
                                                        </h3>
                                                        <span class="m-widget1__desc">
                                                        Awerage Weekly Profit
                                                        </span>
                                                    </div>
                                                    <div class="col m--align-right">
                                                        <span class="m-widget1__number m--font-brand">
                                                        +$17,800
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col">
                                                        <h3 class="m-widget1__title">
                                                            Orders
                                                        </h3>
                                                        <span class="m-widget1__desc">
                                                        Weekly Customer Orders
                                                        </span>
                                                    </div>
                                                    <div class="col m--align-right">
                                                        <span class="m-widget1__number m--font-danger">
                                                        +1,800
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="m-widget1__item">
                                                <div class="row m-row--no-padding align-items-center">
                                                    <div class="col">
                                                        <h3 class="m-widget1__title">
                                                            Issue Reports
                                                        </h3>
                                                        <span class="m-widget1__desc">
                                                        System bugs and issues
                                                        </span>
                                                    </div>
                                                    <div class="col m--align-right">
                                                        <span class="m-widget1__number m--font-success">
                                                        -27,49%
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        end:: Widgets/Stats2-1
                                    </div>
                                    <div class="col-xl-4">
                                        begin:: Widgets/Daily Sales
                                        <div class="m-widget14">
                                            <div class="m-widget14__header m--margin-bottom-30">
                                                <h3 class="m-widget14__title">
                                                    Daily Sales
                                                </h3>
                                                <span class="m-widget14__desc">
                                                Check out each collumn for more details
                                                </span>
                                            </div>
                                            <div class="m-widget14__chart" style="height:120px;">
                                                <canvas id="m_chart_daily_sales"></canvas>
                                            </div>
                                        </div>
                                        end:: Widgets/Daily Sales
                                    </div>
                                    <div class="col-xl-4">
                                        begin:: Widgets/Profit Share
                                        <div class="m-widget14">
                                            <div class="m-widget14__header">
                                                <h3 class="m-widget14__title">
                                                    Profit Share
                                                </h3>
                                                <span class="m-widget14__desc">
                                                Profit Share between customers
                                                </span>
                                            </div>
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div id="m_chart_profit_share" class="m-widget14__chart" style="height: 160px">
                                                        <div class="m-widget14__stat">
                                                            45
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="m-widget14__legends">
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                                            <span class="m-widget14__legend-text">
                                                            37% Sport Tickets
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                                            <span class="m-widget14__legend-text">
                                                            47% Business Events
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                                            <span class="m-widget14__legend-text">
                                                            19% Others
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        end:: Widgets/Profit Share
                                    </div>
                                </div>
                            </div>
                        </div>
 --}}
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="m-portlet m-portlet--full-height ">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">
                                                    Domain Renewals
                                                    &nbsp;<span class="badge badge-info" title="renewals this month">{{ count($domains) }}</span>
                                                    &nbsp;<span class="badge badge-warning" title="renewals this week">{{ $renewals['domains']['thisWeek'] }}</span>
                                                    &nbsp;<span class="badge badge-danger" title="renewals today">{{ $renewals['domains']['today'] }}</span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools">

                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        begin::m-widget4
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Domain name</th>
                                                    <th class="text-right">Renewal in</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($domains as $key => $domain)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td><a href="domains/{{ $domain->id }}">{{ $domain->name }}</a></td>
                                                    <td class="text-right">
                                                        <?php $expDt = Carbon::createFromFormat('Y-m-d', $domain->expiry_date); ?>
                                                        @if ($expDt->isToday())
                                                            <span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-warning"></i>&nbsp;Today</span>
                                                        @else
                                                        {{ $expDt->diffForHumans(null, true) }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>


                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="m-portlet m-portlet--full-height ">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">
                                                    Hosting Renewals
                                                    &nbsp;<span class="badge badge-info" title="renewals this month">{{ count($hostings) }}</span>
                                                    &nbsp;<span class="badge badge-warning" title="renewals this week">{{ $renewals['hostings']['thisWeek'] }}</span>
                                                    &nbsp;<span class="badge badge-danger" title="renewals today">{{ $renewals['hostings']['today'] }}</span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools">

                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        begin::m-widget4
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Website name</th>
                                                    <th class="text-right">Renewal in</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($hostings as $key => $hosting)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td><a href="hostings/{{ $hosting->id }}">{{ $hosting->website->name }}</a></td>
                                                    <td class="text-right">
                                                        <?php $expDt = Carbon::createFromFormat('Y-m-d', $hosting->expiry_date); ?>
                                                        @if ($expDt->isToday())
                                                            <span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-warning"></i>&nbsp;Today</span>
                                                        @else
                                                        {{ $expDt->diffForHumans(null, true) }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>


                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="m-portlet m-portlet--full-height ">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">
                                                    SSL Renewals
                                                    &nbsp;<span class="badge badge-info" title="renewals this month">{{ count($ssls) }}</span>
                                                    &nbsp;<span class="badge badge-warning" title="renewals this week">{{ $renewals['ssls']['thisWeek'] }}</span>
                                                    &nbsp;<span class="badge badge-danger" title="renewals today">{{ $renewals['ssls']['today'] }}</span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools">

                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        begin::m-widget4
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Website Name</th>
                                                    <th class="text-right">Renewal in</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($ssls as $key => $ssl)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td><a href="ssls/{{ $ssl->id }}">{{ $ssl->website->name }}</a></td>
                                                    <td class="text-right">
                                                        <?php $expDt = Carbon::createFromFormat('Y-m-d', $ssl->expiry_date); ?>
                                                        @if ($expDt->isToday())
                                                            <span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-warning"></i>&nbsp;Today</span>
                                                        @else
                                                        {{ $expDt->diffForHumans(null, true) }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>


                                    </div>
                                </div>
                            </div>

                            <div class="col-xl-6">
                                <div class="m-portlet m-portlet--full-height ">
                                    <div class="m-portlet__head">
                                        <div class="m-portlet__head-caption">
                                            <div class="m-portlet__head-title">
                                                <h3 class="m-portlet__head-text">
                                                    Maintenance Renewals
                                                    &nbsp;<span class="badge badge-info" title="renewals this month">{{ count($maintenances) }}</span>
                                                    &nbsp;<span class="badge badge-warning" title="renewals this week">{{ $renewals['maintenances']['thisWeek'] }}</span>
                                                    &nbsp;<span class="badge badge-danger" title="renewals today">{{ $renewals['maintenances']['today'] }}</span>
                                                </h3>
                                            </div>
                                        </div>
                                        <div class="m-portlet__head-tools">

                                        </div>
                                    </div>
                                    <div class="m-portlet__body">
                                        begin::m-widget4
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Website name</th>
                                                    <th class="text-right">Renewal in</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                            @foreach ($maintenances as $key => $maintenance)
                                                <tr>
                                                    <td>{{ ++$key }}</td>
                                                    <td><a href="maintenances/{{ $maintenance->id }}">{{ $maintenance->website->name }}</a></td>
                                                    <td class="text-right">
                                                        <?php $expDt = Carbon::createFromFormat('Y-m-d', $maintenance->expiry_date); ?>
                                                        @if ($expDt->isToday())
                                                            <span class="m-badge m-badge--danger m-badge--wide"><i class="fa fa-warning"></i>&nbsp;Today</span>
                                                        @else
                                                        {{ $expDt->diffForHumans(null, true) }}
                                                        @endif
                                                    </td>
                                                </tr>
                                            </tbody>
                                            @endforeach
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
@stop