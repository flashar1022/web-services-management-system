@extends('layouts.dash-wrap')


@section('content')

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">SSLs</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/ssls" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/ssls/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Edit SSL</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/ssls/{{ $ssl->id }}" method="POST">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Website
								</label>
								<div class="col-6">
									<select class="form-control" id="hosting_website_id" name="website_id" required="">
										<option value=""><strong>Select a website</strong></option>
								@foreach ($websites as $key => $website)
									@if ($key == $ssl->website->id)
										<option selected value="{{ $key }}">{{ $website }}</option>
									@else
										<option value="{{ $key }}">{{ $website }}</option>
									@endif
								@endforeach

									</select>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Domain
								</label>
								<div class="col-6">
									<select class="form-control" id="hosting_domain_id" name="domain_id" required="">
										<option value=""><strong>Select a website first</strong></option>

								@foreach ($domains as $key => $domain)
									@if ($key == $ssl->domain->id)
										<option selected value="{{ $key }}">{{ $domain }}</option>
									@else
										<option value="{{ $key }}">{{ $domain }}</option>
									@endif
								@endforeach

									</select>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Provider Name</label>
								<div class="col-6">
									<input type="text" class="form-control" id="provider_name" name="provider_name" placeholder="Provider Name" required="" value="{{ $ssl->provider_name }}" >
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">SSL Details</label>
								<div class="col-6">
									<textarea class="form-control" id="details" name="details" placeholder="SSL Details e.g. DV, Wildcard SSL for 1 Year" required="">{{ $ssl->details }}</textarea>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Purchase Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="purchase_date" name="purchase_date" placeholder="Purchase Date" required="" value="{{ $ssl->purchase_date }}" >
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Expiry Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="expiry_date" name="expiry_date" placeholder="Expiry Date" required="" value="{{ $ssl->expiry_date }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">SSL Control Panel URL</label>
								<div class="col-6">
									<input type="url" class="form-control" id="control_panel_url" name="control_panel_url" placeholder="SSL Control Panel URL" required="" value="{{ $ssl->control_panel_url }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">SSL Control Panel Username</label>
								<div class="col-6">
									<input type="text" class="form-control" id="control_panel_username" name="control_panel_username" placeholder="SSL Control Panel Username" required="" value="{{ $ssl->control_panel_username }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">SSL Control Panel Password</label>
								<div class="col-6">
									<input type="text" class="form-control" id="control_panel_pass" name="control_panel_pass" placeholder="SSL Control Panel Password" required="" value="{{ $ssl->control_panel_pass }}">
								</div>
							</div>
						</div>{{-- m-portlet__body END --}}

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-3"></div>
									<div class="col-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>
					</form>
				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>

	<script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
	<script>
	$(document).ready(function() {

	    $('select[name="website_id"]').on('change', function(){
	        var websiteId = $(this).val();
			var domainList = $('select[name="domain_id"]');

	        if(websiteId != '') {
	            $.ajax({
	                url: '/domains/get/'+websiteId,
	                type:"GET",
	                dataType:"json",
	                beforeSend: function(){
	                    $('#loader').css("visibility", "visible");
	                },

	                success:function(data) {
						if($(data).toArray().length){
	                    	domainList.empty().append('<option value="">Select a domain</option>').attr('disabled', false);
						} else {
	                    	domainList.empty().append('<option value="">There are no domains for this website</option>').attr('disabled', true);
						}

	                    $.each(data, function(key, value){
	                        domainList.append('<option value="'+ key +'">' + value + '</option>');
	                    });
	                },
	                complete: function(){
	                    $('#loader').css("visibility", "hidden");
	                }
	            });
	        } else {
	            domainList.empty().append('<option value="" selected><strong>Select a website first</strong></option>').attr('disabled', true);
	        }

	    });
	});
	</script>

@endsection