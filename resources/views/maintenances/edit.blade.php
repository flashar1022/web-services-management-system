@extends('layouts.dash-wrap')


@section('content')


	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Maintenances</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/maintenances" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/maintenances/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Edit Maintenance</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/maintenances/{{ $maintenance->id}}" method="POST">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">

							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Website (Client Name)
								</label>
								<div class="col-6">
									<select class="form-control" id="maintenance_website_id" name="website_id">
										<option selected {{ $maintenance->website->id }} value="{{ $maintenance->website->id }}">{{ $maintenance->website->name. ' (' . $maintenance->website->client->name . ')'}}</option>

									@foreach ($websites as $website)
										<option value="{{ $website->id }}">{{ $website->name . ' (' . $website->client->name . ')'}}</option>
									@endforeach

									</select>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Maintenance Details</label>
								<div class="col-6">
									<textarea class="form-control" id="details" name="details" placeholder="Maintenance Details" required="">{{ $maintenance->details }}</textarea>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Start Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="start_date" name="start_date" placeholder="Start Date" required="" value="{{ $maintenance->start_date }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Expiry Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="expiry_date" name="expiry_date" placeholder="Expiry Date" required="" value="{{ $maintenance->expiry_date }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Admin URL</label>
								<div class="col-6">
									<input type="url" class="form-control" id="admin_url" name="admin_url" placeholder="Admin URL" required="" value="{{ $maintenance->admin_url }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Admin Username</label>
								<div class="col-6">
									<input type="text" class="form-control" id="admin_username" name="admin_username" placeholder="Admin Username" required="" value="{{ $maintenance->admin_username }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Admin Password</label>
								<div class="col-6">
									<input type="text" class="form-control" id="admin_pass" name="admin_pass" placeholder="Admin Password" required="" value="{{ $maintenance->admin_pass }}">
								</div>
							</div>
						</div>

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-3"></div>
									<div class="col-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>

					</form>
				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>


@endsection