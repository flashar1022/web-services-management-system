@extends('layouts.dash-wrap')

@section('content')
<?php use Carbon\Carbon; ?>
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Maintenances</h3>
						<div class="d-inline-block">
							<a href="/maintenances" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
							<a href="/maintenances/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
						</div>
					</div>
				</div>
			</div>

			<div class="m-content">
				<div class="row">

					<div class="col-lg-12">

	@if (session('status'))
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
    	    {{ session('status') }}
    	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	</div>
	@endif

						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Maintenances' List
										</h3>
									</div>
								</div>
							</div>

							<div class="m-portlet__body">

								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Website Name</th>
											<th>Maintenance Details</th>
											<th>Start Date</th>
											<th>Expiry&nbsp;Date</th>
											<th>Actions</th>
										</tr>
									</thead>

									<tbody>

										@if (count($maintenances))

											@foreach ($maintenances as $maintenance)

											@php
												$expDt = Carbon::createFromFormat('Y-m-d', $maintenance->expiry_date);

												if($expDt < Carbon::today()){
													$tdClass = 'bg-danger text-white';
													$trClass = 'table-danger';

												} else if ($expDt->isToday()){
													$tdClass = 'bg-warning';
													$trClass = 'table-warning';
												} else {
													$tdClass = '';
													$trClass = '';
												}
											@endphp

											<tr class="{{ $trClass }}">
												<td class="{{ $tdClass }}">{{ ++$i }}</td>
												<td><a href="/websites/{{ $maintenance->website->id }}">{{ $maintenance->website->name }}</a></td>
												<td>{{ str_limit($maintenance->details, 60) }}</td>
												<td>{{ $maintenance->start_date }}</td>

												@if ($expDt->isToday())
													<td class="text-center"><span class="m-badge m-badge--warning m-badge--wide text-white">Today</span>
                                                @elseif($expDt < Carbon::today())
                                                    <td class="text-center"><span class="m-badge m-badge--danger m-badge--wide">Expired {{ $expDt->diffForHumans() }}</span>
                                                @else
                                                    <td>{{ $maintenance->expiry_date }}
                                                @endif
												</td>

												<td>
													<a href="/maintenances/{{ $maintenance->id }}"><i class="fa fa-eye"></i></a>
													<a href="/maintenances/{{ $maintenance->id }}/edit">&nbsp;<i class="fa fa-pencil"></i>&nbsp;</a>
													<form action="/maintenances/{{ $maintenance->id }}" method="POST" class="d-inline" id="delete-maintenance-{{ $maintenance->id }}">
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<a href="#" onclick="deleteMaintenance({{ $maintenance->id }}, '{{ $maintenance->website->name }}')"><i class="fa fa-trash"></i></a>
													</form>
												</td>
											</tr>

											@endforeach

											<script>
												function deleteMaintenance(deleteID, websiteName) {
													console.log('clicked : ' + deleteID + ' : ' + websiteName);
													r = confirm('Are you sure you want to delete \'' + websiteName + ' maintenance\'?');
													if (r == true) {
														document.getElementById('delete-maintenance-' + deleteID).submit();
													} else {
														console.log('cancelled');
													}
												}
											</script>

										@else

											<tr>
												<td colspan="6"><div class="alert alert-danger">{{ 'No Maintenance records found' }}</div></td>
											</tr>

										@endif

									</tbody>
								</table>

								{!! $maintenances->render() !!}

							</div>
						</div> {{-- m-portlet END --}}

					</div>
				</div>
			</div>

@endsection