@extends('layouts.dash-wrap')


@section('content')
	{{-- {{ dd($maintenance->all()) }} --}}

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Maintenance Details</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/maintenances" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/maintenances/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">

					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">{{ $maintenance->website->name }} Maintenance Details</h3>
							</div>
						</div>
					</div>

					<div class="m-portlet__body">

						<dl class="row">

							<dt class="col-3 text-right">Website Name</dt>
							<dd class="col-9"><a href="/websites/{{ $maintenance->website->id }}">{{ $maintenance->website->name }}</a></dd>

							<dt class="col-3 text-right">Client Name</dt>
							<dd class="col-9"><a href="/clients/{{ $maintenance->website->client->id }}">{{ $maintenance->website->client->name }}</a></dd>

							<dt class="col-3 text-right">Maintenance Details</dt>
							<dd class="col-9">{{ $maintenance->details }}</dd>

							<dt class="col-3 text-right">Start Date</dt>
							<dd class="col-9">{{ $maintenance->start_date }}</dd>

							<dt class="col-3 text-right">Expiry Date</dt>
							<dd class="col-9">{{ $maintenance->expiry_date }}</dd>

							<dt class="col-3 text-right">Admin URL</dt>
							<dd class="col-9"><a href="{{ $maintenance->admin_url }}" title="External Link">{{ $maintenance->admin_url }}&nbsp; <i class="fa fa-external-link"></i></a></dd>

							<dt class="col-3 text-right">Admin Username</dt>
							<dd class="col-9">{{ $maintenance->admin_username }}</dd>

							<dt class="col-3 text-right">Admin Password</dt>
							<dd class="col-9">{{ $maintenance->admin_pass }}</dd>
						</dl>
					</div><!-- div.m-portlet__body END-->

					<div class="m-portlet__foot ">

						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<a href="/maintenances/{{  $maintenance->id }}/edit" class="btn btn-warning">Edit</a>
								&nbsp;
								<form action="/maintenances/{{  $maintenance->id }}" method="POST" class="m-form m-form--label-align-right d-inline" id="delete-maintenance-{{ $maintenance->id }}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<a href="#" onclick="deleteMaintenance()" class="btn btn-danger">Delete</a>
								</form>
								<script>
									function deleteMaintenance() {
										r = confirm('Are you sure you want to delete \'{{ $maintenance->website->name }} maintenance\'?');
										if (r == true) {
											document.getElementById('delete-maintenance-{{ $maintenance->id }}').submit();
										} else {
											console.log('cancelled');
										}
									}
								</script>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

@stop