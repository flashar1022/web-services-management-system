@extends('layouts.dash-wrap')


@section('content')

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Hosting Details</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/hostings" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/hostings/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">

					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">{{ $hosting->domain->name }} Hosting Details</h3>
							</div>
						</div>
					</div>

					<div class="m-portlet__body">

						<dl class="row">
							<dt class="col-3 text-right">Package Name</dt>
							<dd class="col-9">{{ $hosting->package_name }}</dd>

							<dt class="col-3 text-right">Package Details</dt>
							<dd class="col-9">{{ $hosting->package_details }}</dd>

							<dt class="col-3 text-right"><!-- Website/  -->Domain</dt>
							<dd class="col-9"><!-- <a href="/websites/{{-- {{ $hosting->website->id }} --}}">{{-- {{ $hosting->website->name }} --}}</a>/  --><a href="/domains/{{ $hosting->domain->id}}">{{ $hosting->domain->name }}</a></dd>

							<dt class="col-3 text-right">Purchase Date</dt>
							<dd class="col-9">{{ $hosting->purchase_date }}</dd>

							<dt class="col-3 text-right">Expiry Date</dt>
							<dd class="col-9">{{ $hosting->expiry_date }}</dd>

							<dt class="col-3 text-right">Email Accounts</dt>
							<dd class="col-9">
								@if ($hosting->email_accounts)
									<span class="m-badge m-badge--success m-badge--wide">Yes</span>
								@else
									<span class="m-badge m-badge--danger m-badge--wide">No</span>
								@endif
							</dd>

							<dt class="col-3 text-right">Disk Space</dt>
							<dd class="col-9">{{ $hosting->disk_space }} GBs</dd>

							<dt class="col-3 text-right">Bandwidth</dt>
							<dd class="col-9">{{ $hosting->bandwidth }} GBs</dd>

							<dt class="col-3 text-right">Nameserver 1</dt>
							<dd class="col-9">{{ $hosting->nameserver_1 }}</dd>

							<dt class="col-3 text-right">Nameserver 2</dt>
							<dd class="col-9">{{ $hosting->nameserver_2 }}</dd>

							<dt class="col-3 text-right">Control Panel URL</dt>
							<dd class="col-9"><a href="{{ $hosting->control_panel_url }}" title="External Link">{{ $hosting->control_panel_url }}&nbsp; <i class="fa fa-external-link"></i></a></dd>

							<dt class="col-3 text-right">Control Panel Username</dt>
							<dd class="col-9">{{ $hosting->control_panel_username }}</dd>

							<dt class="col-3 text-right">Control Panel Password</dt>
							<dd class="col-9">{{ $hosting->control_panel_pass }}</dd>
						</dl>
					</div><!-- div.m-portlet__body END-->

					<div class="m-portlet__foot ">

						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<a href="/hostings/{{  $hosting->id }}/edit" class="btn btn-warning">Edit</a>
								&nbsp;
								<form action="/hostings/{{  $hosting->id }}" method="POST" class="m-form m-form--label-align-right d-inline" id="delete-hosting-{{ $hosting->id }}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<a href="#" onclick="deleteHosting()" class="btn btn-danger">Delete</a>
								</form>
								<script>
									function deleteHosting() {
										r = confirm('Are you sure you want to delete \'{{ $hosting->website->name }} hosting\'?');
										if (r == true) {
											document.getElementById('delete-hosting-{{ $hosting->id }}').submit();
										} else {
											console.log('cancelled');
										}
									}
								</script>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

@stop