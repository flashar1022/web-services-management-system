@extends('layouts.dash-wrap')


@section('content')

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Hostings</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/hostings" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/hostings/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Edit Hosting</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/hostings/{{ $hosting->id }}" method="POST">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Package Name</label>
								<div class="col-6">
									<input type="text" class="form-control" id="package_name" name="package_name" placeholder="Package Name" required="" value="{{ $hosting->package_name }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Package Details</label>
								<div class="col-6">
									<textarea class="form-control" id="package_details" name="package_details" placeholder="Package Details" required="">{{ $hosting->package_details }}</textarea>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Website
								</label>
								<div class="col-6">
									<select class="form-control" id="hosting_website_id" name="website_id" required="">
										<option value=""><strong>Select a website</strong></option>
								@foreach ($websites as $key => $website)
									@if ($key == $hosting->website_id)
										<option selected value="{{ $key }}">{{ $website }}</option>
									@else
										<option value="{{ $key }}">{{ $website }}</option>
									@endif
								@endforeach
									</select>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Domain
								</label>
								<div class="col-6">
									<select class="form-control" id="hosting_domain_id" name="domain_id" required="">
										<option value=""><strong>Select a website first</strong></option>
									@foreach ($domains as $key => $domain)
										@if ($key == $hosting->domain->id)
											<option selected value="{{ $key }}">{{ $domain }}</option>
										@else
											<option value="{{ $key }}">{{ $domain }}</option>
										@endif
									@endforeach
									</select>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Purchase Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="purchase_date" name="purchase_date" placeholder="Purchase Date" required="" value="{{ $hosting->purchase_date }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Expiry Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="expiry_date" name="expiry_date" placeholder="Expiry Date" required="" value="{{ $hosting->expiry_date }}">
								</div>
							</div>


							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Email Accounts</label>
								<div class="col-6">
									<div class="m-checkbox-inline">
										<label class="m-checkbox">
											<input type="checkbox" id="email_accounts" name="email_accounts" {{ $hosting->email_accounts? 'checked' : '' }}>
											<span></span>
										</label>
									</div>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Disk Space (GBs)</label>
								<div class="col-6">
									<input type="number" class="form-control" id="disk_space" name="disk_space" placeholder="Disk Space" required="" value="{{ $hosting->disk_space }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Bandwidth (GBs)</label>
								<div class="col-6">
									<input type="number" class="form-control" id="bandwidth" name="bandwidth" placeholder="Bandwidth" required="" value="{{ $hosting->bandwidth }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Nameserver 1</label>
								<div class="col-6">
									<input type="text" class="form-control" id="nameserver_1" name="nameserver_1" placeholder="Nameserver 1" required="" value="{{ $hosting->nameserver_1 }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Nameserver 2</label>
								<div class="col-6">
									<input type="text" class="form-control" id="nameserver_2" name="nameserver_2" placeholder="Nameserver 2" required="" value="{{ $hosting->nameserver_2 }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Control Panel URL</label>
								<div class="col-6">
									<input type="url" class="form-control" id="control_panel_url" name="control_panel_url" placeholder="Control Panel URL" required="" value="{{ $hosting->control_panel_url }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Control Panel Username</label>
								<div class="col-6">
									<input type="text" class="form-control" id="control_panel_username" name="control_panel_username" placeholder="Control Panel Username" required="" value="{{ $hosting->control_panel_username }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Control Panel Password</label>
								<div class="col-6">
									<input type="text" class="form-control" id="control_panel_pass" name="control_panel_pass" placeholder="Control Panel Password" required="" value="{{ $hosting->control_panel_pass }}">
								</div>
							</div>
						</div>

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-3"></div>
									<div class="col-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>
					</form>

				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>

	<script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
	<script>
	$(document).ready(function() {

	    $('select[name="website_id"]').on('change', function(){
	        var websiteId = $(this).val();
			var domainList = $('select[name="domain_id"]');

	        if(websiteId != '') {
	            $.ajax({
	                url: '/domains/get/'+websiteId,
	                type:"GET",
	                dataType:"json",
	                beforeSend: function(){
	                    $('#loader').css("visibility", "visible");
	                },

	                success:function(data) {
						if($(data).toArray().length){
	                    	domainList.empty().append('<option value="">Select a domain</option>').attr('disabled', false);
						} else {
	                    	domainList.empty().append('<option value="">There are no domains for this website</option>').attr('disabled', true);
						}

	                    $.each(data, function(key, value){
	                        domainList.append('<option value="'+ key +'">' + value + '</option>');
	                    });
	                },
	                complete: function(){
	                    $('#loader').css("visibility", "hidden");
	                }
	            });
	        } else {
	            domainList.empty().append('<option value="" selected><strong>Select a website first</strong></option>').attr('disabled', true);
	        }
	    });
	});
	</script>

@endsection