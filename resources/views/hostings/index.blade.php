@extends('layouts.dash-wrap')

@section('content')
<?php use Carbon\Carbon; ?>
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Hostings</h3>
						<div class="d-inline-block">
							<a href="/hostings" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
							<a href="/hostings/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
						</div>
					</div>
				</div>
			</div>

			<div class="m-content">
				<div class="row">

					<div class="col-lg-12">

	@if (session('status'))
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
    	    {{ session('status') }}
    	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	</div>
	@endif

						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Hostings' List
										</h3>
									</div>
								</div>
							</div>

							<div class="m-portlet__body">

								<table class="table m-table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Domain</th>
											<th>Website</th>
											<th>Package Name</th>
											<th>Package Details</th>
											{{-- <th>Nameservers</th> --}}
											<th>Purchase Date</th>
											<th>Expiry Date</th>
											<th>Actions</th>
										</tr>
									</thead>

									<tbody>

										@if (count($hostings))

											@foreach ($hostings as $hosting)

											@php
												$expDt = Carbon::createFromFormat('Y-m-d', $hosting->expiry_date);

												if($expDt < Carbon::today()){
													$tdClass = 'bg-danger text-white';
													$trClass = 'table-danger';

												} else if ($expDt->isToday()){
													$tdClass = 'bg-warning';
													$trClass = 'table-warning';
												} else {
													$tdClass = '';
													$trClass = '';
												}
											@endphp

											<tr class="{{ $trClass }}">
												<td class="{{ $tdClass }}">{{ ++$i }}</td>
												<td>
													<a href="/domains/{{ $hosting->domain->id }}">{{ $hosting->domain->name }}</a>
												</td>
												<td>
													<a href="/websites/{{ $hosting->website->id }}">{{ $hosting->website->name }}</a><br>
												</td>
												<td>{{ $hosting->package_name }}</td>
												<td>
													<small><em>Disk Space:</em> {{ $hosting->disk_space }} GBs<br />
														<em>Bandwidth:</em> {{ $hosting->bandwidth }} GBs<br />
														<em>Email Accounts:</em> {{ $hosting->email_accounts ? 'Yes' : 'No'}}</small>
												</td>
												{{-- <td>{{ $hosting->nameserver_1 }}<br />{{ $hosting->nameserver_2 }}</td> --}}
												<td>{{ $hosting->purchase_date }}</td>

												@if ($expDt->isToday())
													<td class="text-center"><span class="m-badge m-badge--warning m-badge--wide text-white">Today</span>
                                                @elseif($expDt < Carbon::today())
                                                    <td class="text-center"><span class="m-badge m-badge--danger m-badge--wide">Expired {{ $expDt->diffForHumans() }}</span>
                                                @else
                                                    <td>{{ $hosting->expiry_date }}
                                                @endif
												</td>

												<td>
													<a href="/hostings/{{ $hosting->id }}"><i class="fa fa-eye"></i></a>
													<a href="/hostings/{{ $hosting->id }}/edit">&nbsp;<i class="fa fa-pencil"></i>&nbsp;</a>
													<form action="/hostings/{{ $hosting->id }}" method="POST" class="d-inline" id="delete-hosting-{{ $hosting->id }}">
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<a href="#" onclick="deleteHosting({{ $hosting->id }}, '{{ $hosting->website->name }}')"><i class="fa fa-trash"></i></a>
													</form>
												</td>
											</tr>
											@endforeach

											<script>
												function deleteHosting(deleteID, hostingWebsiteName) {
													console.log("clicked "+ deleteID +" : "+ hostingWebsiteName)
													r = confirm('Are you sure you want to delete \'' + hostingWebsiteName + ' hosting\'?');
													if (r == true) {
														document.getElementById('delete-hosting-' + deleteID).submit();
													} else {
														console.log('cancelled');
													}
												}
											</script>

										@else

											<tr>
												<td colspan="8"><div class="alert alert-danger">{{ 'No Hosting records found' }}</div></td>
											</tr>

										@endif

									</tbody>
								</table>

								{!! $hostings->render() !!}

							</div>
						</div> {{-- m-portlet END --}}

					</div>
				</div>
			</div>

@endsection