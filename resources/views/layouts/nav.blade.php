<?php

   $curView = app('request')->path();

   $activeLink = [
      'dash' => '',
      'clients' => '',
      'websites' => '',
      'domains' => '',
      'hostings' => '',
      'maintenances' => '',
      'ssls' => '',
      'settings' => '',
      'users' => ''
   ];
?>

@switch($curView)
   @case('home')
        <?php $activeLink['dash'] = 'm-menu__item--active' ?>
        @break;
   @case('clients')
        <?php $activeLink['clients'] = 'm-menu__item--active' ?>
        @break;
   @case('websites')
        <?php $activeLink['websites'] = 'm-menu__item--active' ?>
        @break;
   @case('domains')
        <?php $activeLink['domains'] = 'm-menu__item--active' ?>
        @break;
   @case('hostings')
        <?php $activeLink['hostings'] = 'm-menu__item--active' ?>
        @break;
   @case('maintenances')
        <?php $activeLink['maintenances'] = 'm-menu__item--active' ?>
        @break;
   @case('ssls')
        <?php $activeLink['ssls'] = 'm-menu__item--active' ?>
        @break;
   @case('settings')
        <?php $activeLink['settings'] = 'm-menu__item--active' ?>
        @break;
   @case('users')
        <?php $activeLink['users'] = 'm-menu__item--active' ?>
        @break;
@endswitch


                <button class="m-aside-left-close m-aside-left-close--skin-dark " id="m_aside_left_close_btn">
                <i class="la la-close"></i>
                </button>
                <div id="m_aside_left" class="m-grid__item m-aside-left m-aside-left--skin-dark ">
                    <!-- BEGIN: Aside Menu -->
                    <div
                        id="m_ver_menu"
                        class="m-aside-menu m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                        data-menu-vertical="true"
                        data-menu-scrollable="false" data-menu-dropdown-timeout="500"
                        >
                        <ul class="m-menu__nav m-menu__nav--dropdown-submenu-arrow ">
                            <li class="m-menu__item {{ $activeLink['dash'] }}" aria-haspopup="true" >
                                <a href="{{ URL::to('/') }}" class="m-menu__link " title="Dashboard">
                                <i class="m-menu__link-icon flaticon-line-graph"></i>
                                <span class="m-menu__link-title">
                                <span class="m-menu__link-wrap">
                                <span class="m-menu__link-text">
                                Dashboard
                                </span>
                                </span>
                                </span>
                                </a>
                            </li>
                            <li class="m-menu__section">
                                <h4 class="m-menu__section-text">
                                    Resources
                                </h4>
                                <i class="m-menu__section-icon flaticon-more-v3"></i>
                            </li>
                            <li class="m-menu__item {{ $activeLink['clients'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/clients" class="m-menu__link" title="Clients">
                                    <i class="m-menu__link-icon fa fa-handshake-o"></i>
                                    <span class="m-menu__link-text">Clients</span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ $activeLink['websites'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/websites" class="m-menu__link" title="Websites">
                                    <i class="m-menu__link-icon fa fa-sitemap"></i>
                                    <span class="m-menu__link-text">Websites</span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ $activeLink['domains'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/domains" class="m-menu__link" title="Domains">
                                    <i class="m-menu__link-icon fa fa-globe"></i>
                                    <span class="m-menu__link-text">Domains</span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ $activeLink['hostings'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/hostings" class="m-menu__link" title="Hostings">
                                    <i class="m-menu__link-icon fa fa-server"></i>
                                    <span class="m-menu__link-text">Hostings</span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ $activeLink['maintenances'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/maintenances" class="m-menu__link" title="Maintenances">
                                    <i class="m-menu__link-icon fa fa-desktop"></i>
                                    <span class="m-menu__link-text">Maintenances</span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ $activeLink['ssls'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/ssls" class="m-menu__link" title="SSLs">
                                    <i class="m-menu__link-icon fa fa-lock"></i>
                                    <span class="m-menu__link-text">SSLs</span>
                                </a>
                            </li>
                            <li class="m-menu__item {{ $activeLink['settings'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/settings" class="m-menu__link" title="Settings">
                                    <i class="m-menu__link-icon fa fa-cog"></i>
                                    <span class="m-menu__link-text">Settings</span>
                                </a>
                            </li>
                            {{-- <li class="m-menu__item {{ $activeLink['users'] }}" aria-haspopup="true" data-menu-submenu-toggle="hover">
                                <a href="{{ URL::to('/') }}/users" class="m-menu__link" title="Settings">
                                    <i class="m-menu__link-icon fa fa-users"></i>
                                    <span class="m-menu__link-text">Users</span>
                                </a>
                            </li> --}}
                        </ul>
                    </div>
                    <!-- END: Aside Menu -->
                </div>