<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        {{-- <title>{{ config('app.name', 'Websites Management | Welcome!') }}</title> --}}

        <title>Websites Management | Welcome!</title>

        <meta name="description" content="">
        <!--begin::Web font -->
        <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
        <script>
          WebFont.load({
            google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
          });
        </script>

        <!--begin::Base Styles -->
        <link href="/assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
        <link href="/assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />

        <link rel="shortcut icon" href="/assets/demo/default/media/img/logo/favicon.ico" />

        {{-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}

        <style type="text/css">
            .m-login__container .m-login__logo img {
                max-height: 150px;
            }
        </style>

    </head>

    <body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
        <!-- begin:: Page -->
        <div class="m-grid m-grid--hor m-grid--root m-page">
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--singin m-login--2 m-login-2--skin-1" id="m_login" style="background-image: url(/assets/app/media/img//bg/bg-1.jpg);">
                <div class="m-grid__item m-grid__item--fluid    m-login__wrapper">
                    <div class="m-login__container">
                        <div class="m-login__logo">
                            <a href="#">
                                <img src="{{ asset('assets/webpro.png') }}">
                            </a>
                        </div>

                        @yield('content')

                        @guest
                            <div class="m-login__account">
                                <span class="m-login__account-msg">
                                    Don't have an account yet ?
                                </span>
                                &nbsp;&nbsp;

                                {{-- <a href="{{ route('register') }}" id="m_login_signup" class="m-link m-link--light m-login__account-link"> --}}

                                <a href="{{ route('register') }}" id="" class="m-link m-link--light m-login__account-link">
                                    Sign Up
                                </a>
                            </div>
                        @endguest

                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Page -->
        <!--begin::Base Scripts -->
        <script src="/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
        <script src="/assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Base Scripts -->
        <!--begin::Page Snippets -->
        <script src="/assets/snippets/pages/user/login.js" type="text/javascript"></script>
        <!--end::Page Snippets -->
    </body>
    <!-- end::Body -->
</html>


<!--
            {{-- @if (Route::has('login')) --}}
                <div class="top-right links">
                    {{-- @auth --}}
                        {{-- <a href="{{ url('/home') }}">Home</a> --}}
                    {{-- @else --}}
                        {{-- <a href="{{ route('login') }}">Login</a> --}}
                        {{-- <a href="{{ route('register') }}">Register</a> --}}
                    {{-- @endauth --}}
                </div>
            {{-- @endif --}}
 

            {{-- {{ Auth::user()->name }} --}}

            {{--<a href="{{ route('logout') }}"
                onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                Logout
            </a>--}}

            {{-- <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>--}}
 -->

