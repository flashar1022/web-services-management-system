@extends('layouts.dash-wrap')


@section('content')
<script src="{{ asset('js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>

<?php use Carbon\Carbon; ?>
                    <!-- BEGIN: Subheader -->
                    <div class="m-subheader ">
                        <div class="d-flex align-items-center">
                            <div class="mr-auto">
                                <h3 class="m-subheader__title ">
                                    Dashboard
                                </h3>
                            </div>
                            <div>
                                <span class="m-subheader__daterange" id="m_dashboard_daterangepicker">
                                <span class="m-subheader__daterange-label">
                                <span class="m-subheader__daterange-title"></span>
                                <span class="m-subheader__daterange-date m--font-brand"></span>
                                </span>

                                {{-- Day selector button --}}

                                <!-- <a href="#" class="btn btn-sm btn-brand m-btn m-btn--icon m-btn--icon-only m-btn--custom m-btn--pill">
                                <i class="la la-angle-down"></i>
                                </a> -->
                                </span>
                            </div>
                        </div>
                    </div>
                    <!-- END: Subheader -->

                    <div class="m-content">

                        <div class="m-portlet">
                            <div class="m-portlet__body  m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">
                                    <div class="col-xl-4">
                                        <!--begin:: Widgets/Revenue Change-->
                                        <div class="m-widget14">
                                            <div class="m-widget14__header">
                                                <h3 class="m-widget14__title">
                                                    App Statistics
                                                </h3>
                                                <span class="m-widget14__desc">
                                                    Total records
                                                </span>
                                            </div>
                                            <div class="row  align-items-center">
                                                <div class="col">
                                                    <div id="app-stats" class="m-widget14__chart1" style="height: 180px"></div>
                                                </div>
                                                <div class="col">
                                                    <div class="m-widget14__legends">
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-primary"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $stats['clients']['total'] }} Clients
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-success"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $stats['websites']['total'] }} Websites
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $stats['domains']['total'] }} Domains
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-info"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $stats['ssls']['total'] }} SSLs
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $stats['hostings']['total'] }} Hostings
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-danger"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $stats['amcs']['total'] }} AMCs
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end:: Widgets/Revenue Change-->
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="col-md-12">
                                            <!--begin::Total Profit-->
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Total Domains
                                                    </h4>
                                                    <br>
                                                    <span class="m-widget24__desc">
                                                        Expired Domains: {{ $stats['domains']['expired'] }}
                                                    </span>
                                                    <span class="m-widget24__stats m--font-brand">
                                                        {{ $stats['domains']['total'] }}
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-brand" role="progressbar" style="width: {{ $stats['domains']['exp_percent'] }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Expired
                                                    </span>
                                                    <span class="m-widget24__number">
                                                        {{ $stats['domains']['exp_percent'] }}%
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Total SSLs
                                                    </h4>
                                                    <br>
                                                    <span class="m-widget24__desc">
                                                        Expired SSLs: {{ $stats['ssls']['expired'] }}
                                                    </span>
                                                    <span class="m-widget24__stats m--font-info">
                                                        {{ $stats['ssls']['total'] }}
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-info" role="progressbar" style="width: {{ $stats['domains']['exp_percent'] }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Expired
                                                    </span>
                                                    <span class="m-widget24__number">
                                                        {{ $stats['domains']['exp_percent'] }}%
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xl-4">
                                        <div class="col-md-12">
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Total Hostings
                                                    </h4>
                                                    <br>
                                                    <span class="m-widget24__desc">
                                                        Expired Hostings: {{ $stats['hostings']['expired'] }}
                                                    </span>
                                                    <span class="m-widget24__stats m--font-danger">
                                                        {{ $stats['hostings']['total'] }}
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-danger" role="progressbar" style="width: {{ $stats['hostings']['exp_percent'] }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Expired
                                                    </span>
                                                    <span class="m-widget24__number">
                                                        {{ $stats['hostings']['exp_percent'] }}%
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="m-widget24">
                                                <div class="m-widget24__item">
                                                    <h4 class="m-widget24__title">
                                                        Total AMCs
                                                    </h4>
                                                    <br>
                                                    <span class="m-widget24__desc">
                                                        Expired AMCs: {{ $stats['amcs']['expired'] }}
                                                    </span>
                                                    <span class="m-widget24__stats m--font-success">
                                                        {{ $stats['amcs']['total'] }}
                                                    </span>
                                                    <div class="m--space-10"></div>
                                                    <div class="progress m-progress--sm">
                                                        <div class="progress-bar m--bg-success" role="progressbar" style="width: {{ $stats['domains']['exp_percent'] }}%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>
                                                    </div>
                                                    <span class="m-widget24__change">
                                                        Expired
                                                    </span>
                                                    <span class="m-widget24__number">
                                                        {{ $stats['domains']['exp_percent'] }}%
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- BEGIN: chart widget -->
                        <div class="m-portlet">
                            <div class="m-portlet__body  m-portlet__body--no-padding">
                                <div class="row m-row--no-padding m-row--col-separator-xl">

                                @for ($i=1; $i < 5; $i++)

                                @switch($i)

                                    @case(1)
                                        <?php $entity = 'Domain'; ?>
                                        @break
                                    @case(2)
                                        <?php $entity = 'SSL'; ?>
                                        @break
                                    @case(3)
                                        <?php $entity = 'Hosting'; ?>
                                        @break
                                    @case(4)
                                        <?php $entity = 'Maintenance'; ?>
                                        @break
                                @endswitch

                                    <div class="col-xl-3">
                                        <div class="m-widget14">
                                            <div class="m-widget14__header">
                                                <h3 class="m-widget14__title">
                                                    {{ $entity }} renewals
                                                </h3>
                                                <!-- <span class="m-widget14__desc">
                                                    Profit Share between customers
                                                </span> -->
                                            </div>
                                            <div class="row  align-items-center">
                                                <div class="col">
                                                    <div id="{{ $entity }}-renewals" class="m-widget14__chart" style="height: 160px">
                                                        <div class="m-widget14__stat">
                                                            {{ $renewals[$i]['thisMonth'] }}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="m-widget14__legends">
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-brand"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $renewals[$i]['thisMonth'] }} This month
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-accent"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $renewals[$i]['thisWeek'] }} This week
                                                            </span>
                                                        </div>
                                                        <div class="m-widget14__legend">
                                                            <span class="m-widget14__legend-bullet m--bg-warning"></span>
                                                            <span class="m-widget14__legend-text">
                                                                {{ $renewals[$i]['today'] }} Today
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <script type="text/javascript">
                                    $(document).ready(function(){

                                        var chart = new Chartist.Pie('#{{ $entity }}-renewals', {
                                            series: [{
                                                    value: {{ $renewals[$i]['thisMonth'] }},
                                                    className: 'custom',
                                                    meta: {
                                                        color: mUtil.getColor('brand')
                                                    }
                                                },
                                                {
                                                    value: {{ $renewals[$i]['thisWeek'] }},
                                                    className: 'custom',
                                                    meta: {
                                                        color: mUtil.getColor('accent')
                                                    }
                                                },
                                                {
                                                    value: {{ $renewals[$i]['today'] }},
                                                    className: 'custom',
                                                    meta: {
                                                        color: mUtil.getColor('warning')
                                                    }
                                                }
                                            ],
                                            labels: [1, 2, 3]
                                        }, {
                                            donut: true,
                                            donutWidth: 10,
                                            showLabel: false
                                        });

                                        chart.on('draw', function(data) {
                                            if (data.type === 'slice') {
                                                // Get the total path length in order to use for dash array animation
                                                var pathLength = data.element._node.getTotalLength();

                                                // Set a dasharray that matches the path length as prerequisite to animate dashoffset
                                                data.element.attr({
                                                    'stroke-dasharray': pathLength + 'px ' + pathLength + 'px'
                                                });

                                                // Create animation definition while also assigning an ID to the animation for later sync usage
                                                var animationDefinition = {
                                                    'stroke-dashoffset': {
                                                        id: 'anim' + data.index,
                                                        dur: 1000,
                                                        from: -pathLength + 'px',
                                                        to: '0px',
                                                        easing: Chartist.Svg.Easing.easeOutQuint,
                                                        // We need to use `fill: 'freeze'` otherwise our animation will fall back to initial (not visible)
                                                        fill: 'freeze',
                                                        'stroke': data.meta.color
                                                    }
                                                };

                                                // If this was not the first slice, we need to time the animation so that it uses the end sync event of the previous animation
                                                if (data.index !== 0) {
                                                    animationDefinition['stroke-dashoffset'].begin = 'anim' + (data.index - 1) + '.end';
                                                }

                                                // We need to set an initial value before the animation starts as we are not in guided mode which would do that for us

                                                data.element.attr({
                                                    'stroke-dashoffset': -pathLength + 'px',
                                                    'stroke': data.meta.color
                                                });

                                                // We can't use guided mode as the animations need to rely on setting begin manually
                                                // See http://gionkunz.github.io/chartist-js/api-documentation.html#chartistsvg-function-animate
                                                data.element.animate(animationDefinition, false);
                                            }
                                        });

                                        // For the sake of the example we update the chart every time it's created with a delay of 8 seconds
                                        chart.on('created', function() {
                                            if (window.__anim21278907124) {
                                                clearTimeout(window.__anim21278907124);
                                                window.__anim21278907124 = null;
                                            }
                                            window.__anim21278907124 = setTimeout(chart.update.bind(chart), 15000);
                                        });
                                    });
                                    </script>

                                @endfor

                                </div>
                            </div>
                        </div>
                        <!-- END: chart widget -->
                    </div>

<script type="text/javascript">
    $(document).ready(function(){
        Morris.Donut({
            element: 'app-stats',
            data: [
                {
                    label: "Clients",
                    value: {{ $stats['clients']['total'] }}
                },
                {
                    label: "Websites",
                    value: {{ $stats['websites']['total'] }}
                },
                {
                    label: "Domains",
                    value: {{ $stats['domains']['total'] }}
                },
                {
                    label: "Ssls",
                    value: {{ $stats['ssls']['total'] }}
                },
                {
                    label: "Hostings",
                    value: {{ $stats['hostings']['total'] }}
                },
                {
                    label: "Amcs",
                    value: {{ $stats['amcs']['total'] }}
                }
            ],
            colors: [
                mUtil.getColor('primary'),
                mUtil.getColor('success'),
                mUtil.getColor('warning'),
                mUtil.getColor('info'),
                mUtil.getColor('danger'),
                mUtil.getColor('brand')
            ],
        });
    });
</script>


@stop