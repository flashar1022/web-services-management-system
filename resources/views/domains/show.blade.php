@extends('layouts.dash-wrap')


@section('content')
	{{-- {{dd($domain)}} --}}

	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Domain Details</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/domains" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/domains/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">

					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<h3 class="m-portlet__head-text">{{ $domain->name }}</h3>
							</div>
						</div>
					</div>

					<div class="m-portlet__body">

						<dl class="row">
							<dt class="col-3 text-right">Domain Name</dt>
							<dd class="col-9">{{ $domain->name }}</dd>

							<dt class="col-3 text-right">Website Name</dt>
							<dd class="col-9"><a href="/websites/{{ $domain->website->id }}">{{ $domain->website->name }}</a></dd>

							<dt class="col-3 text-right">Client Name</dt>
							<dd class="col-9"><a href="/clients/{{ $domain->website->client->id }}">{{ $domain->website->client->name }}</a></dd>

							<dt class="col-3 text-right">Purchase Date</dt>
							<dd class="col-9">{{ $domain->purchase_date }}</dd>

							<dt class="col-3 text-right">Expiry Date</dt>
							<dd class="col-9">{{ $domain->expiry_date }}</dd>

							<dt class="col-3 text-right">Registrar</dt>
							<dd class="col-9">{{ $domain->registrar_name }}</dd>

							<dt class="col-3 text-right">Control Panel URL</dt>
							<dd class="col-9"><a href="{{ $domain->control_panel_url }}" title="External Link">{{ $domain->control_panel_url }}&nbsp; <i class="fa fa-external-link" ></i></a></dd>

							<dt class="col-3 text-right">Control Panel Username</dt>
							<dd class="col-9">{{ $domain->control_panel_username }}</dd>

							<dt class="col-3 text-right">Control Panel Password</dt>
							<dd class="col-9">{{ $domain->control_panel_pass }}</dd>
						</dl>
					</div><!-- div.m-portlet__body END-->

					<div class="m-portlet__foot ">

						<div class="row">
							<div class="col-lg-3"></div>
							<div class="col-lg-6">
								<a href="/domains/{{  $domain->id }}/edit" class="btn btn-warning">Edit</a>
								&nbsp;
								<form action="/domains/{{  $domain->id }}" method="POST" class="m-form m-form--label-align-right d-inline" id="delete-domain-{{ $domain->id }}">
									{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<a href="#" onclick="deleteDomain()" class="btn btn-danger">Delete</a>
								</form>
								<script>
									function deleteDomain() {
										r = confirm('Are you sure you want to delete \'{{ $domain->name }}\'?');
										if (r == true) {
											document.getElementById('delete-domain-{{ $domain->id }}').submit();
										} else {
											console.log('cancelled');
										}
									}
								</script>
							</div>
						</div>

					</div>

				</div>
			</div>
		</div>
	</div>

@stop