@extends('layouts.dash-wrap')


@section('content')


	<div class="m-subheader ">
		<div class="d-flex align-items-center">
			<div class="mr-auto">
				<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Domains</h3>
				<div class="d-inline-block">
					{{-- <li class="m-nav__item m-nav__item--home"> --}}
						<a href="/domains" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
						<a href="/domains/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
					{{-- </li> --}}
				</div>
			</div>
		</div>
	</div>

	<div class="m-content">
		<div class="row">

			<div class="col-lg-12">
				<div class="m-portlet">
					<div class="m-portlet__head">
						<div class="m-portlet__head-caption">
							<div class="m-portlet__head-title">
								<span class="m-portlet__head-icon m--hide">
									<i class="la la-gear"></i>
								</span>
								<h3 class="m-portlet__head-text">Edit Domain</h3>
							</div>
						</div>
					</div>

					<form class="m-form m-form--label-align-right" action="/domains/{{ $domain->id }}" method="POST">

						{{ csrf_field() }}
						{{ method_field('PATCH') }}

						<div class="m-portlet__body">
							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Domian Name</label>
								<div class="col-6">
									<input type="text" class="form-control" id="domain_name" name="name" placeholder="e.g. http://example.com" pattern="[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)" required="" value="{{ $domain->name }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-form-label col-3">
									Website (Client Name)
								</label>
								<div class="col-6">
									<select class="form-control" id="" name="website_id">

									@foreach ($websites as $website)

										<option {{ ($website->id == $domain->website->id) ? 'selected' : '' }} value="{{ $website->id }}">{{ $website->name . ' (' . $website->client->name .')' }}</option>

									@endforeach

									</select>
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Purchase Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="purchase_date" name="purchase_date" placeholder="Purchase Date" required="" value="{{ $domain->purchase_date }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Expiry Date</label>
								<div class="col-6">
									<input type="date" class="form-control" id="expiry_date" name="expiry_date" placeholder="Expiry Date" required="" value="{{ $domain->expiry_date }}">
								</div>
							</div>


							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Registrar Name</label>
								<div class="col-6">
									<input type="text" class="form-control" id="registrar_name" name="registrar_name" placeholder="Registrar Name" required="" value="{{ $domain->registrar_name }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Control Panel URL</label>
								<div class="col-6">
									<input type="url" class="form-control" id="control_panel_url" name="control_panel_url" placeholder="Control Panel URL" required="" value="{{ $domain->control_panel_url }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Control Panel Username</label>
								<div class="col-6">
									<input type="text" class="form-control" id="control_panel_username" name="control_panel_username" placeholder="Control Panel Username" required="" value="{{ $domain->control_panel_username }}">
								</div>
							</div>

							<div class="form-group m-form__group row">
								<label class="col-3 col-form-label">Control Panel Password</label>
								<div class="col-6">
									<input type="text" class="form-control" id="control_panel_pass" name="control_panel_pass" placeholder="Control Panel Password" required="" value="{{ $domain->control_panel_pass }}">
								</div>
							</div>
						</div>

			            <div class="m-portlet__foot m-portlet__foot--fit">
							<div class="m-form__actions m-form__actions">
								<div class="row">
									<div class="col-3"></div>
									<div class="col-6">
										<button type="submit" class="btn btn-success">Update</button>&nbsp;
										<button type="reset" class="btn btn-outline-danger">Reset</button>
									</div>
								</div>
							</div>
			            </div>
					</form>

				</div>


				@if (count($errors))

					<div class="alert alert-danger">
						<ul>
							@foreach ($errors->all() as $error)

								<li>{{ $error }}</li>

							@endforeach
						</ul>
					</div>

				@endif

			</div>


		</div>
	</div>


@endsection