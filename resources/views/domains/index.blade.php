@extends('layouts.dash-wrap')

@section('content')
<?php use Carbon\Carbon; ?>
			<div class="m-subheader ">
				<div class="d-flex align-items-center">
					<div class="mr-auto">
						<h3 class="m-subheader__title m-subheader__title--separator d-inline-block">Domains</h3>
						<div class="d-inline-block">
							<a href="/domains" class="btn btn-primary"><i class="fa fa-list"></i>&nbsp; List</a>
							<a href="/domains/create" class="btn btn-info"><i class="fa fa-plus"></i>&nbsp; Add</a>
						</div>
					</div>
				</div>
			</div>

			<div class="m-content">
				<div class="row">

					<div class="col-lg-12">

	@if (session('status'))
    	<div class="alert alert-success alert-dismissible fade show" role="alert">
    	    {{ session('status') }}
    	    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    	</div>
	@endif

						<div class="m-portlet">
							<div class="m-portlet__head">
								<div class="m-portlet__head-caption">
									<div class="m-portlet__head-title">
										<span class="m-portlet__head-icon m--hide">
										<i class="la la-gear"></i>
										</span>
										<h3 class="m-portlet__head-text">
											Domains' List
										</h3>
									</div>
								</div>
							</div>

							<div class="m-portlet__body">

								<table class="table table-bordered">
									<thead>
										<tr>
											<th>#</th>
											<th>Domain Name</th>
											<th>Website Name</th>
											<th>Purchase Date</th>
											<th>Expiry Date</th>
											<th>Domain Registrar</th>
											<th>Actions</th>
										</tr>
									</thead>

									<tbody>

										@if (count($domains))

											@foreach ($domains as $domain)

											@php
												$expDt = Carbon::createFromFormat('Y-m-d', $domain->expiry_date);

												if($expDt < Carbon::today()){
													$tdClass = 'bg-danger text-white';
													$trClass = 'table-danger';

												} else if ($expDt->isToday()){
													$tdClass = 'bg-warning';
													$trClass = 'table-warning';
												} else {
													$tdClass = '';
													$trClass = '';
												}
											@endphp

											<tr class="{{ $trClass }}">
												<td class="{{ $tdClass }}">{{ ++$i }}</td>
												<td>{{ $domain->name }}</td>
												<td><a href="/websites/{{ $domain->website->id }}">{{ $domain->website->name }}</a></td>
												<td>{{ $domain->purchase_date }}</td>

												@if ($expDt->isToday())
													<td class="text-center"><span class="m-badge m-badge--warning m-badge--wide text-white">Today</span>
                                                @elseif($expDt < Carbon::today())
                                                    <td class="text-center"><span class="m-badge m-badge--danger m-badge--wide">Expired {{ $expDt->diffForHumans() }}</span>
                                                @else
                                                    <td>{{ $domain->expiry_date }}
                                                @endif

												<td>{{ str_limit($domain->registrar_name, 20) }}</td>
												<td>
													<a href="/domains/{{ $domain->id }}"><i class="fa fa-eye"></i></a>
													<a href="/domains/{{ $domain->id }}/edit">&nbsp;<i class="fa fa-pencil"></i>&nbsp;</a>
													<form action="/domains/{{ $domain->id }}" method="POST" class="d-inline" id="delete-domain-{{ $domain->id }}">
														{{ csrf_field() }}
														{{ method_field('DELETE') }}
														<a href="#" onclick="deleteDomain({{ $domain->id }}, '{{ $domain->name }}')"><i class="fa fa-trash"></i></a>
													</form>

												</td>
											</tr>
											@endforeach

											<script>
												function deleteDomain(domainID, domainName) {
													console.log('clicked ' + domainID + " : " + domainName);
													r = confirm('Are you sure you want to delete \'' + domainName + '\'?');
													if (r == true) {
														document.getElementById('delete-domain-' + domainID).submit();
													} else {
														console.log('cancelled');
													}
												}
											</script>
										@else

											<tr>
												<td colspan="7"><div class="alert alert-danger">{{ 'No domain records found' }}</div></td>
											</tr>

										@endif

									</tbody>
								</table>

							</div>
						</div> {{-- m-portlet END --}}

					</div>
				</div>
			</div>

@endsection